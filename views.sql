/* Aktivni_korisnici */
DROP VIEW IF EXISTS Aktivni_korisnici;
CREATE VIEW Aktivni_korisnici AS
SELECT KorisnickoIme, ImeIPrezime, Email, LozinkaZaPristupSistemu, DatumRegistracije
FROM Osoba
NATURAL JOIN Korisnik
WHERE Aktivan = 1
WITH CHECK OPTION;

/* Admin - For login*/
DROP VIEW IF EXISTS Admin;
CREATE VIEW Admin AS
SELECT KorisnickoIme, ImeIPrezime, LozinkaZaPristupSistemu, IDOsobe
FROM Osoba
NATURAL JOIN korisnik
NATURAL JOIN administrator

/* Messages*/
DROP VIEW IF EXISTS Messages;
CREATE VIEW Messages AS
SELECT KorisnickoIme, ImeIPrezime, Vrijeme, Naziv AS Knjiga, Sadrzaj,Email,IDOsobe
FROM Poruka P
NATURAL JOIN korisnik
NATURAL JOIN osoba
LEFT JOIN knjiga K ON P.IDKnjige = K.IDKnjige
WHERE Odgovorena = 0 OR Odgovorena is null
ORDER BY Vrijeme

/* AdministratorView*/
DROP VIEW IF EXISTS AdministratorView;
CREATE VIEW AdministratorView AS
SELECT *
FROM Osoba
NATURAL JOIN korisnik
NATURAL JOIN kontakt_osoba
NATURAL JOIN administrator

/*/////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*TRIGERI*/

/* Kada se brise admin zadatke koji su zaduzeni za njega prebacuje na admina sa najmanje zadatakar*/
DROP TRIGGER IF EXISTS prebaci_zadatke;
DELIMITER $$

CREATE TRIGGER prebaci_zadatke BEFORE DELETE ON osoba 
FOR EACH ROW 
BEGIN
DECLARE newAdminId INT;
DECLARE newAdminKorisnickoIme VARCHAR(255);
DECLARE minZad DOUBLE;
SET newAdminId = (SELECT IDOsobe FROM(
	SELECT A.IDOsobe, COUNT(K.IDZadatka) mycount FROM je_zaduzen_za K 
	RIGHT JOIN administrator A ON A.IDOsobe = K.IDOsobe GROUP BY A.IDOsobe ORDER BY mycount ASC) P LIMIT 1);
SET newAdminKorisnickoIme = (SELECT A.KorisnickoIme FROM administrator A WHERE A.IDOsobe = newAdminId LIMIT 1);
UPDATE je_zaduzen_za J SET J.IDOsobe = newAdminId, j.KorisnickoIme = newAdminKorisnickoIme WHERE J.IDOsobe = OLD.IDOsobe;
END $$
DELIMITER ;

/*Kada se doda nova posjeta za knjigu, ukoliko nije administrator uvecaj "BrojPosjeta" za knjigu*/
DROP TRIGGER IF EXISTS broj_posjeta;
DELIMITER $$

CREATE TRIGGER broj_posjeta AFTER INSERT ON posjetio
FOR EACH ROW
BEGIN
    DECLARE isAdmin int;
    SET isAdmin = (SELECT COUNT(IDOsobe) FROM
                   (SELECT * FROM administrator WHERE administrator.IDOsobe = NEW.IDOsobe) A);
    IF(isAdmin = 0) THEN 
        UPDATE knjiga K SET K.BrojPosjeta = k.BrojPosjeta + 1 WHERE k.IDKnjige = NEW.IDKnjige;
    END IF;
END $$
DELIMITER ;

/*/////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*PROCEDURE*/

/* Dodaj rijec ali samo ako je njeno pocetno vrijeme manje od pocetnog vremena za prvu rijec */
DROP PROCEDURE IF EXISTS dodaj_rijec;
DELIMITER $$

CREATE PROCEDURE dodaj_rijec(
    IN Rijec VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
    IN Pocetak TIME,
    IN RedniBroj INT,
    IN IDKnjige INT,
    OUT status INT)
BEGIN
	DECLARE rbPrethodne INT;
    DECLARE pocetakPrethodne TIME;
	IF(RedniBroj > 0) THEN
		SET rbPrethodne = RedniBroj - 1;
		SET pocetakPrethodne = 
			(SELECT R.Pocetak FROM rijec R WHERE R.RedniBroj = rbPrethodne AND R.IDKnjige = IDKnjige LIMIT 1);
	ELSE
		SET pocetakPrethodne = 0;
	END IF;
    IF(RedniBroj = 0 OR (TIME_TO_SEC(Pocetak) - TIME_TO_SEC(pocetakPrethodne)) > 0) THEN 
    	INSERT INTO rijec
        VALUES(NULL,Rijec,Pocetak,RedniBroj,NULL,IDKnjige);
        SET status = 1;
    ELSE
    	SET status = 0;
    END IF;
END $$
DELIMITER ;
        
 /* Obrisi knjigu - kada brises knjige, brisi sve njene rijeci osim ako je njihov 
 Spektogram razlicit od null. U tom slucaju samo postavi IDKnjige na 0.*/       
DROP PROCEDURE IF EXISTS obrisi_knjigu;
DELIMITER $$

CREATE PROCEDURE obrisi_knjigu(
	IN Knjiga INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE idR INT;
	DECLARE sR BLOB; 
	DECLARE rijeci CURSOR FOR SELECT R.IDRijeci,R.Spektogram FROM rijec R WHERE R.IDKnjige = Knjiga;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN rijeci;
	petlja: LOOP
		FETCH rijeci INTO idR,sR;
		IF done THEN 
			LEAVE petlja;
		END IF;
		IF sR IS NULL THEN
			DELETE FROM `rijec` WHERE `rijec`.`IDRijeci` = idR;
		ELSE
			UPDATE `rijec` SET `IDKnjige` = '0' WHERE `rijec`.`IDRijeci` = idR;
		END IF;
	END LOOP;
	CLOSE rijeci;
	DELETE FROM `knjiga` WHERE `knjiga`.`IDKnjige` = Knjiga;
END $$
DELIMITER ;
















