﻿using bProjektni.data.dto;
using bProjektni.data.dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dao.MySqlDAO;
using System.Windows.Forms;

namespace bProjektni.view_login
{
    public  class Login 
    {
        public AdminDTO bpLogin(string username, string pass)
        {
            AdminDAO admin = new MySqlAdminDAO();
            AdminDTO user = new AdminDTO();
            List<AdminDTO> users = new List<AdminDTO>();
            user.KorisnickoIme = username;
            

            users = admin.getAdmins(user);
            if (users.Count == 1)
            {
                if(users[0].Lozinka == pass.GetHashCode().ToString())
                {
                    return users[0];
                }
                
            }
            
             return null;
            
        }
        
        
    }
}
