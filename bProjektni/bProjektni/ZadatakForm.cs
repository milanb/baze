﻿using bProjektni.data.dao;
using bProjektni.data.dao.MySqlDAO;
using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni
{
    public partial class ZadatakForm : Form
    {
        enum Mod { save,update };

        int idOsobe;
        ZadatakDTO zadatakDto;
        Mod trenutniMod;
        //constructors
        public ZadatakForm()
        {
            InitializeComponent();
        }
        public ZadatakForm(int idOsobe) : this()
        {
            this.idOsobe = idOsobe;
            this.zadatakDto = new ZadatakDTO();
            zadatakDto.DatumPocetka = dpPocetka.Value;
            zadatakDto.Rok = dpRok.Value;
            bindZadatak(zadatakDto);
            trenutniMod = Mod.save;
            btnDelete.Visible = false;

        }
        public ZadatakForm(int idOsobe,ZadatakDTO zadatakDto) : this()
        {
            this.idOsobe = idOsobe;
            this.zadatakDto = zadatakDto;
            bindZadatak(zadatakDto);
            trenutniMod = Mod.update;
        }

        private void bindZadatak(ZadatakDTO zadatakDto)
        {
            txtOpis.DataBindings.Add("Text", zadatakDto, "Opis");
            dpPocetka.DataBindings.Add("Value", zadatakDto, "DatumPocetka");
            dpRok.DataBindings.Add("Value", zadatakDto, "Rok");
            chbZavrsen.DataBindings.Add("Checked", zadatakDto, "Zavrsen");
            chbBlokiran.DataBindings.Add("Checked", zadatakDto, "Blokiran");
            txtBrPredvidjenih.DataBindings.Add("Text", zadatakDto, "BrPredvidjenihH");
            txtBrUtrosenih.DataBindings.Add("Text", zadatakDto, "BrUtrosenihH");
            txtBrPotrebnih.DataBindings.Add("Text", zadatakDto, "BrPotebnihH");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ZadatakDAO zadatakDao = new MySqlZadatakDAO();
            if (trenutniMod == Mod.update)
            {
                if (zadatakDao.update(zadatakDto))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Doslo je do nekakve greske! Izvinjavamo se!");
                }

            }
            if (trenutniMod == Mod.save)
            {
                if (zadatakDao.add(zadatakDto, idOsobe))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Doslo je do nekakve greske! Izvinjavamo se!");
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ZadatakDAO zadatakDao = new MySqlZadatakDAO();
            zadatakDao.delete(zadatakDto.IdZadatka);
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCnacel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
