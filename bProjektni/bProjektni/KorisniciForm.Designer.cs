﻿namespace bProjektni
{
    partial class KorisniciForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KorisniciForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvOsobe = new System.Windows.Forms.DataGridView();
            this.gbKontakPodaci = new System.Windows.Forms.GroupBox();
            this.lbTelefoni = new System.Windows.Forms.ListBox();
            this.txtTelelfon = new System.Windows.Forms.TextBox();
            this.btnTelefon = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.gbKorisnickiPodaci = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKorisnickoIme = new System.Windows.Forms.TextBox();
            this.txtLozinka = new System.Windows.Forms.TextBox();
            this.gbTipKorisnika = new System.Windows.Forms.GroupBox();
            this.rbAdministrator = new System.Windows.Forms.RadioButton();
            this.rbKontaktOsoba = new System.Windows.Forms.RadioButton();
            this.rbKorisnik = new System.Windows.Forms.RadioButton();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtImeIPrezime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNewUser = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOsobe)).BeginInit();
            this.gbKontakPodaci.SuspendLayout();
            this.gbKorisnickiPodaci.SuspendLayout();
            this.gbTipKorisnika.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvOsobe);
            this.splitContainer1.Panel1.Controls.Add(this.btnNewUser);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbKontakPodaci);
            this.splitContainer1.Panel2.Controls.Add(this.gbKorisnickiPodaci);
            this.splitContainer1.Panel2.Controls.Add(this.gbTipKorisnika);
            this.splitContainer1.Panel2.Controls.Add(this.txtEmail);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.txtImeIPrezime);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.Controls.Add(this.btnDelete);
            this.splitContainer1.Panel2.Controls.Add(this.btnUpdate);
            this.splitContainer1.Panel2.Controls.Add(this.btnSave);
            this.splitContainer1.Size = new System.Drawing.Size(688, 487);
            this.splitContainer1.SplitterDistance = 263;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvOsobe
            // 
            this.dgvOsobe.AllowUserToAddRows = false;
            this.dgvOsobe.AllowUserToDeleteRows = false;
            this.dgvOsobe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOsobe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOsobe.Location = new System.Drawing.Point(0, 0);
            this.dgvOsobe.Name = "dgvOsobe";
            this.dgvOsobe.ReadOnly = true;
            this.dgvOsobe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOsobe.Size = new System.Drawing.Size(263, 464);
            this.dgvOsobe.TabIndex = 0;
            this.dgvOsobe.Click += new System.EventHandler(this.dgvOsobe_Click);
            // 
            // gbKontakPodaci
            // 
            this.gbKontakPodaci.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbKontakPodaci.Controls.Add(this.lbTelefoni);
            this.gbKontakPodaci.Controls.Add(this.txtTelelfon);
            this.gbKontakPodaci.Controls.Add(this.btnTelefon);
            this.gbKontakPodaci.Controls.Add(this.label6);
            this.gbKontakPodaci.Controls.Add(this.label5);
            this.gbKontakPodaci.Controls.Add(this.txtAdresa);
            this.gbKontakPodaci.Location = new System.Drawing.Point(10, 253);
            this.gbKontakPodaci.Name = "gbKontakPodaci";
            this.gbKontakPodaci.Size = new System.Drawing.Size(399, 193);
            this.gbKontakPodaci.TabIndex = 8;
            this.gbKontakPodaci.TabStop = false;
            this.gbKontakPodaci.Text = "Kontakt Podaci";
            this.gbKontakPodaci.Visible = false;
            // 
            // lbTelefoni
            // 
            this.lbTelefoni.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTelefoni.FormattingEnabled = true;
            this.lbTelefoni.Location = new System.Drawing.Point(137, 93);
            this.lbTelefoni.Name = "lbTelefoni";
            this.lbTelefoni.Size = new System.Drawing.Size(256, 95);
            this.lbTelefoni.TabIndex = 11;
            // 
            // txtTelelfon
            // 
            this.txtTelelfon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelelfon.Location = new System.Drawing.Point(137, 56);
            this.txtTelelfon.Name = "txtTelelfon";
            this.txtTelelfon.Size = new System.Drawing.Size(142, 20);
            this.txtTelelfon.TabIndex = 10;
            this.txtTelelfon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTelelfon_KeyDown);
            // 
            // btnTelefon
            // 
            this.btnTelefon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTelefon.Location = new System.Drawing.Point(285, 53);
            this.btnTelefon.Name = "btnTelefon";
            this.btnTelefon.Size = new System.Drawing.Size(108, 23);
            this.btnTelefon.TabIndex = 9;
            this.btnTelefon.Text = "Dodtaj Telefon";
            this.btnTelefon.UseVisualStyleBackColor = true;
            this.btnTelefon.Click += new System.EventHandler(this.btnTelefon_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Telefon";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Adresa";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdresa.Location = new System.Drawing.Point(133, 24);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(260, 20);
            this.txtAdresa.TabIndex = 8;
            this.txtAdresa.TextChanged += new System.EventHandler(this.txtImeIPrezime_TextChanged);
            // 
            // gbKorisnickiPodaci
            // 
            this.gbKorisnickiPodaci.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbKorisnickiPodaci.Controls.Add(this.label4);
            this.gbKorisnickiPodaci.Controls.Add(this.label3);
            this.gbKorisnickiPodaci.Controls.Add(this.txtKorisnickoIme);
            this.gbKorisnickiPodaci.Controls.Add(this.txtLozinka);
            this.gbKorisnickiPodaci.Location = new System.Drawing.Point(10, 147);
            this.gbKorisnickiPodaci.Name = "gbKorisnickiPodaci";
            this.gbKorisnickiPodaci.Size = new System.Drawing.Size(399, 100);
            this.gbKorisnickiPodaci.TabIndex = 8;
            this.gbKorisnickiPodaci.TabStop = false;
            this.gbKorisnickiPodaci.Text = "Korisnički Podaci";
            this.gbKorisnickiPodaci.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(83, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Lozinka";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Korisničko ime";
            // 
            // txtKorisnickoIme
            // 
            this.txtKorisnickoIme.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKorisnickoIme.Location = new System.Drawing.Point(133, 19);
            this.txtKorisnickoIme.Name = "txtKorisnickoIme";
            this.txtKorisnickoIme.Size = new System.Drawing.Size(260, 20);
            this.txtKorisnickoIme.TabIndex = 6;
            this.txtKorisnickoIme.TextChanged += new System.EventHandler(this.txtImeIPrezime_TextChanged);
            // 
            // txtLozinka
            // 
            this.txtLozinka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLozinka.Location = new System.Drawing.Point(133, 45);
            this.txtLozinka.Name = "txtLozinka";
            this.txtLozinka.Size = new System.Drawing.Size(260, 20);
            this.txtLozinka.TabIndex = 7;
            this.txtLozinka.TextChanged += new System.EventHandler(this.txtImeIPrezime_TextChanged);
            // 
            // gbTipKorisnika
            // 
            this.gbTipKorisnika.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTipKorisnika.Controls.Add(this.rbAdministrator);
            this.gbTipKorisnika.Controls.Add(this.rbKontaktOsoba);
            this.gbTipKorisnika.Controls.Add(this.rbKorisnik);
            this.gbTipKorisnika.Location = new System.Drawing.Point(10, 82);
            this.gbTipKorisnika.Name = "gbTipKorisnika";
            this.gbTipKorisnika.Size = new System.Drawing.Size(399, 59);
            this.gbTipKorisnika.TabIndex = 7;
            this.gbTipKorisnika.TabStop = false;
            this.gbTipKorisnika.Text = "Tip Korisnika";
            // 
            // rbAdministrator
            // 
            this.rbAdministrator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbAdministrator.AutoSize = true;
            this.rbAdministrator.Location = new System.Drawing.Point(190, 20);
            this.rbAdministrator.Name = "rbAdministrator";
            this.rbAdministrator.Size = new System.Drawing.Size(85, 17);
            this.rbAdministrator.TabIndex = 5;
            this.rbAdministrator.TabStop = true;
            this.rbAdministrator.Text = "Administrator";
            this.rbAdministrator.UseVisualStyleBackColor = true;
            // 
            // rbKontaktOsoba
            // 
            this.rbKontaktOsoba.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbKontaktOsoba.AutoSize = true;
            this.rbKontaktOsoba.Location = new System.Drawing.Point(88, 20);
            this.rbKontaktOsoba.Name = "rbKontaktOsoba";
            this.rbKontaktOsoba.Size = new System.Drawing.Size(96, 17);
            this.rbKontaktOsoba.TabIndex = 4;
            this.rbKontaktOsoba.TabStop = true;
            this.rbKontaktOsoba.Text = "Kontakt Osoba";
            this.rbKontaktOsoba.UseVisualStyleBackColor = true;
            // 
            // rbKorisnik
            // 
            this.rbKorisnik.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbKorisnik.AutoSize = true;
            this.rbKorisnik.Location = new System.Drawing.Point(20, 20);
            this.rbKorisnik.Name = "rbKorisnik";
            this.rbKorisnik.Size = new System.Drawing.Size(62, 17);
            this.rbKorisnik.TabIndex = 3;
            this.rbKorisnik.TabStop = true;
            this.rbKorisnik.Text = "Korisnik";
            this.rbKorisnik.UseVisualStyleBackColor = true;
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmail.Location = new System.Drawing.Point(143, 38);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(266, 20);
            this.txtEmail.TabIndex = 2;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtImeIPrezime_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Email";
            // 
            // txtImeIPrezime
            // 
            this.txtImeIPrezime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImeIPrezime.Location = new System.Drawing.Point(143, 12);
            this.txtImeIPrezime.Name = "txtImeIPrezime";
            this.txtImeIPrezime.Size = new System.Drawing.Size(266, 20);
            this.txtImeIPrezime.TabIndex = 1;
            this.txtImeIPrezime.TextChanged += new System.EventHandler(this.txtImeIPrezime_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ime i Prezime";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(334, 452);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Clear";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNewUser
            // 
            this.btnNewUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnNewUser.Location = new System.Drawing.Point(0, 464);
            this.btnNewUser.Name = "btnNewUser";
            this.btnNewUser.Size = new System.Drawing.Size(263, 23);
            this.btnNewUser.TabIndex = 3;
            this.btnNewUser.Text = "Add New";
            this.btnNewUser.UseVisualStyleBackColor = true;
            this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(91, 452);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(172, 452);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(253, 452);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // KorisniciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 487);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(704, 526);
            this.Name = "KorisniciForm";
            this.Text = "Korisnici";
            this.Load += new System.EventHandler(this.KorisniciForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOsobe)).EndInit();
            this.gbKontakPodaci.ResumeLayout(false);
            this.gbKontakPodaci.PerformLayout();
            this.gbKorisnickiPodaci.ResumeLayout(false);
            this.gbKorisnickiPodaci.PerformLayout();
            this.gbTipKorisnika.ResumeLayout(false);
            this.gbTipKorisnika.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtImeIPrezime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNewUser;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dgvOsobe;
        private System.Windows.Forms.GroupBox gbTipKorisnika;
        private System.Windows.Forms.RadioButton rbAdministrator;
        private System.Windows.Forms.RadioButton rbKontaktOsoba;
        private System.Windows.Forms.RadioButton rbKorisnik;
        private System.Windows.Forms.GroupBox gbKontakPodaci;
        private System.Windows.Forms.GroupBox gbKorisnickiPodaci;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtKorisnickoIme;
        private System.Windows.Forms.TextBox txtLozinka;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtTelelfon;
        private System.Windows.Forms.Button btnTelefon;
        private System.Windows.Forms.ListBox lbTelefoni;
    }
}