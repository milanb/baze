﻿using bProjektni.data.dao;
using bProjektni.data.dao.MySqlDAO;
using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni
{
    public partial class KorisniciForm : Form
    {
        private DataTable osobe;
        private Mod trenutniMod;
        private TipKorisnika trenutniTip;
        private int idOsobe;
        private int idAdmina;

        enum Mod {nova,izmjena,prikaz,greska};
        enum TipKorisnika { korisnik, kontaktOsoba, administrator,greska };

        public KorisniciForm()
        {
            InitializeComponent();
        }

        public KorisniciForm(int iDOsobe) : this()
        {
            idAdmina = iDOsobe;
        }

        private void setMod(Mod mod)
        {
            trenutniMod = mod;
            switch (mod)
            {
                case Mod.nova:
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    btnNewUser.Enabled = false;
                    btnDelete.Enabled = false;
                    splitContainer1.Panel1.Enabled = false;
                    gbTipKorisnika.Enabled = true;
                    lbTelefoni.Enabled = true;
                    gbKontakPodaci.Visible = false;
                    gbKorisnickiPodaci.Visible = false;
                    splitContainer1.Panel2.Enabled = true;
                    txtTelelfon.Visible = true;
                    btnTelefon.Visible = true;
                    break;

                case Mod.izmjena:
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    btnNewUser.Enabled = false;
                    btnDelete.Enabled = true;
                    splitContainer1.Panel1.Enabled = false;
                    gbTipKorisnika.Enabled = false;
                    lbTelefoni.Enabled = false;
                    splitContainer1.Panel2.Enabled = true;
                    txtTelelfon.Visible = false;
                    btnTelefon.Visible = false;
                    break;

                case Mod.prikaz:
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    btnNewUser.Enabled = true;
                    btnDelete.Enabled = true;
                    splitContainer1.Panel1.Enabled = true;
                    gbTipKorisnika.Enabled = false;
                    lbTelefoni.Enabled = false;
                    splitContainer1.Panel2.Enabled = true;
                    txtTelelfon.Visible = false;
                    btnTelefon.Visible = false;
                    break;

                case Mod.greska:
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;
                    btnNewUser.Enabled = true;
                    btnDelete.Enabled = false;
                    splitContainer1.Panel1.Enabled = true;
                    gbTipKorisnika.Enabled = true;
                    lbTelefoni.Enabled = false;
                    gbKontakPodaci.Visible = false;
                    gbKorisnickiPodaci.Visible = false;
                    splitContainer1.Panel2.Enabled = false;
                    break;
            }
        }

        private void loadOsobe()
        {
            OsobaDAO osobaDao = new MySqlOsobaDAO();
            osobe = osobaDao.dtGetAll();
            dgvOsobe.DataSource = osobe;

            clearAll();
            setMod(Mod.greska);
        }
        void KorisniciForm_Load(object sender, EventArgs e)
        {
            setMod(Mod.greska);

            loadOsobe();
            dgvOsobe.Columns[0].Visible = false;
            btnUpdate.Visible = false;

            rbAdministrator.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            rbKontaktOsoba.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            rbKorisnik.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);

        }

        private void setTipKorisnika()
        {
            idOsobe = Convert.ToInt32(osobe.Rows[dgvOsobe.CurrentCell.RowIndex][0]);
            KorisnikDAO korisnikDao = new MySqlKorisnikDAO();
            KontaktOsobaDAO koDao = new MySqlKontakOsobaDAO();
            bool isKO = koDao.isKontaktOsoba(idOsobe);
            bool isKorisnik = korisnikDao.isKorisnik(idOsobe);

            if (isKO && isKorisnik)
            {
                trenutniTip = TipKorisnika.administrator;
                rbAdministrator.Checked = true;
            }
            else if (isKorisnik)
            {
                trenutniTip = TipKorisnika.korisnik;
                rbKorisnik.Checked = true;
            }
            else if (isKO)
            {
                trenutniTip = TipKorisnika.kontaktOsoba;
                rbKontaktOsoba.Checked = true;
            }
            else
            {
                MessageBox.Show("Desila se greška!");
                trenutniTip = TipKorisnika.greska;
                setMod(Mod.greska);
            }
        }
        private void fillKorisnik(int idOsobe)
        {
            KorisnikDAO korisnikDao = new MySqlKorisnikDAO();
            KorisnikDTO korisnik = korisnikDao.getKorisnik(idOsobe);
            txtKorisnickoIme.Text = korisnik.KorisnickoIme;
        }
        private void fillKO(int idOsobe)
        {
            KontaktOsobaDAO kODAO = new MySqlKontakOsobaDAO();
            KontaktOsobaDTO korisnik = kODAO.getKO(idOsobe);
            txtAdresa.Text = korisnik.Adresa;
            lbTelefoni.DataSource = korisnik.Telefon;
            lbTelefoni.DisplayMember = "BrojTelefona";
            lbTelefoni.SelectionMode = SelectionMode.None;

        }
        private void fillForm()
        {
            txtImeIPrezime.Text = 
                osobe.Rows[dgvOsobe.CurrentCell.RowIndex][1].ToString();
            txtEmail.Text =
                osobe.Rows[dgvOsobe.CurrentCell.RowIndex][2].ToString();

            setTipKorisnika();

            if(trenutniTip == TipKorisnika.administrator)
            {
                fillKO(idOsobe);
                fillKorisnik(idOsobe);
            }else if(trenutniTip == TipKorisnika.kontaktOsoba)
            {
                fillKO(idOsobe);
            }else if(trenutniTip == TipKorisnika.korisnik)
            {
                fillKorisnik(idOsobe);
            }
            

        }

        private void dgvOsobe_Click(object sender, EventArgs e)
        {
            fillForm();
            setMod(Mod.prikaz);
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (rbKorisnik.Checked)
            {
                gbKorisnickiPodaci.Visible = true;
                gbKontakPodaci.Visible = false;
                trenutniTip = TipKorisnika.korisnik;
            }
            else if (rbKontaktOsoba.Checked)
            {
                gbKorisnickiPodaci.Visible = false;
                gbKontakPodaci.Visible = true;
                trenutniTip = TipKorisnika.kontaktOsoba;
            }
            else if (rbAdministrator.Checked)
            {
                gbKorisnickiPodaci.Visible = true;
                gbKontakPodaci.Visible = true;
                trenutniTip = TipKorisnika.administrator;
            }
        }

        private void clearAll()
        {
            txtAdresa.Text = "";
            txtEmail.Text = "";
            txtImeIPrezime.Text = "";
            txtKorisnickoIme.Text = "";
            txtLozinka.Text = "";
            lbTelefoni.DataSource = null;
            rbAdministrator.Checked = false;
            rbKontaktOsoba.Checked = false;
            rbKorisnik.Checked = false;
        }
        private void btnNewUser_Click(object sender, EventArgs e)
        {
            setMod(Mod.nova);
            clearAll();
        }
        private bool saveOsoba()
        {
            OsobaDTO osobaDto = new OsobaDTO();
            osobaDto.Email = txtEmail.Text;
            osobaDto.ImeIPrezime = txtImeIPrezime.Text;
            OsobaDAO osobaDao = new MySqlOsobaDAO();
            idOsobe = osobaDao.add(osobaDto);//MOST IMPORTANT
            if (idOsobe == 0)
            {
                return false;
            }
            return true;
        }
        private bool updateOsoba()
        {
            OsobaDTO osobaDto = new OsobaDTO();
            osobaDto.IdOsobe = idOsobe;
            osobaDto.Email = txtEmail.Text;
            osobaDto.ImeIPrezime = txtImeIPrezime.Text;
            OsobaDAO osobaDao = new MySqlOsobaDAO();
            return osobaDao.update(osobaDto);
        }

        private bool saveKorisnik()
        {
            KorisnikDTO korisnikDto = new KorisnikDTO();
            korisnikDto.IdOsobe = idOsobe;
            korisnikDto.Aktivan = true;
            korisnikDto.KorisnickoIme = txtKorisnickoIme.Text;
            korisnikDto.Lozinka = txtLozinka.Text.GetHashCode().ToString();
            KorisnikDAO korisnikDao = new MySqlKorisnikDAO();
            
            if(korisnikDao.add(korisnikDto) == true)
            {
                return true;
            }
            else
            {
                OsobaDAO osobaDao = new MySqlOsobaDAO();
                osobaDao.delete(idOsobe);
            }
            return false;
        }
        private bool updateKorisnik()
        {
            KorisnikDTO korisnikDto = new KorisnikDTO();
            korisnikDto.IdOsobe = idOsobe;
            korisnikDto.Aktivan = true;
            korisnikDto.KorisnickoIme = txtKorisnickoIme.Text;
            korisnikDto.Lozinka = txtLozinka.Text.GetHashCode().ToString();
            KorisnikDAO korisnikDao = new MySqlKorisnikDAO();

            if(txtLozinka.Text != "")
            {
                korisnikDao.updateLozinka(korisnikDto);
            }
            if (korisnikDao.update(korisnikDto) == true)
            {
                return true;
            }
            else
            {
                OsobaDAO osobaDao = new MySqlOsobaDAO();
                osobaDao.delete(idOsobe);
            }
            return false;
        }

        private bool saveAdministrator()
        {
            KorisnikDAO korisnikDao = new MySqlKorisnikDAO();

            AdministratorDTO adminDto = new AdministratorDTO();
            adminDto.KorisnickoIme = korisnikDao.getKorisnik(idOsobe).KorisnickoIme;
            adminDto.IdOsobe = idOsobe;

            AdministratorDAO adminDao = new MySqlAdministratoDAO();
            return adminDao.add(adminDto);
        }

        private bool saveKontaktOsoba()
        {
            KontaktOsobaDAO kontaktOsovaDao = new MySqlKontakOsobaDAO();

            KontaktOsobaDTO kontaktOsobaDto = new KontaktOsobaDTO();
            kontaktOsobaDto.Adresa = txtAdresa.Text;
            kontaktOsobaDto.IdOsobe = idOsobe;

            kontaktOsovaDao.add(kontaktOsobaDto);

            TelefonDTO telefonDto = new TelefonDTO();
            telefonDto.IdOsobe = idOsobe;
            TelefonDAO telefonDao = new MySqlTelefonDAO();
            try
            {
                foreach (string tel in lbTelefoni.Items)
                {
                    telefonDto.BrojTelefona = tel;
                    telefonDao.add(telefonDto);
                }
            }
            catch (Exception e) { };
            return true;
        }
        private bool updateKontaktOsoba()
        {
            KontaktOsobaDAO kontaktOsovaDao = new MySqlKontakOsobaDAO();

            KontaktOsobaDTO kontaktOsobaDto = new KontaktOsobaDTO();
            kontaktOsobaDto.Adresa = txtAdresa.Text;
            kontaktOsobaDto.IdOsobe = idOsobe;

            return kontaktOsovaDao.update(kontaktOsobaDto);
        }
        private bool save()
        {
            if(!rbAdministrator.Checked && !rbKontaktOsoba.Checked 
                && !rbKorisnik.Checked)//Ako nije nista cekirano
            {
                MessageBox.Show("Niste selektovali tip korisnika");
                return false;
            }
            if(trenutniTip == TipKorisnika.administrator)
            {
                saveOsoba();
                saveKorisnik();
                saveKontaktOsoba();
                saveAdministrator();
            }
            if (trenutniTip == TipKorisnika.korisnik)
            {
                saveOsoba();
                saveKorisnik();
            }

            if (trenutniTip == TipKorisnika.kontaktOsoba)
            {
                saveOsoba();
                saveKontaktOsoba();
            }

            
            loadOsobe();
            return true;
        }
        private bool update()
        {
            if (trenutniTip == TipKorisnika.administrator)
            {
                updateOsoba();
                updateKorisnik();
                updateKontaktOsoba();
                //updateAdministrator();
            }
            if (trenutniTip == TipKorisnika.korisnik)
            {
                updateOsoba();
                updateKorisnik();
            }

            if (trenutniTip == TipKorisnika.kontaktOsoba)
            {
                updateOsoba();
                updateKontaktOsoba();
            }

            loadOsobe();
            return true;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            if(trenutniMod == Mod.nova)
            {
                save();
            }
            else
            {
                update();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            loadOsobe();
        }

        private void btnTelefon_Click(object sender, EventArgs e)
        {
            if (txtTelelfon.Text == "")
            {
                return;
            }
            lbTelefoni.Items.Add(txtTelelfon.Text);
            txtTelelfon.Text = "";
        }

        
        private void txtTelelfon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnTelefon_Click(sender, new EventArgs());
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(trenutniMod != Mod.prikaz)
            {
                return;
            }
            if(idAdmina == idOsobe)
            {
                MessageBox.Show("Ne mozete obrisa sami sebe!");
                return;
            }
            OsobaDAO osobaDao = new MySqlOsobaDAO();
            osobaDao.delete(idOsobe);
            loadOsobe();
        }

        private void txtImeIPrezime_TextChanged(object sender, EventArgs e)
        {
            if(trenutniMod == Mod.prikaz)
            {
                setMod(Mod.izmjena);
            }
        }
    }
}
