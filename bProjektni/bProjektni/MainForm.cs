﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bProjektni.data.dto;
using bProjektni.data.dao;
using bProjektni.data.dao.MySqlDAO;

namespace bProjektni
{
    public partial class MainForm : Form
    { 
        private AdminDTO user;

        public MainForm()
        {
            InitializeComponent();

             //debug
            LoginForm lf = new LoginForm();
            lf.ShowDialog();
            user = lf.user;
            
            /*
            user = new AdminDTO();
            user.ImeIPrezime = "Milan";
            user.IDOsobe = 18;
            */
        }

        public MainForm(AdminDTO user) : this()
        {
            this.user = user;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if(user == null)
            {
                this.Close();
            }
            else
            {
                lblUser.Text = user.ImeIPrezime;
                /*PORUKE*/
                setPoruke();
                /*ZADATCI*/
                setZadatak();
                /*KNJIGE*/
                setKnjige();
                dgvKnjige.Columns[5].Visible = false;
                dgvKnjige.Columns[6].Visible = false;
            }
        }

        private void setKnjige()
        {
            KnjigaDAO knjigeDao = new MySqlKnjigaDAO();
            dgvKnjige.DataSource = knjigeDao.dtGetAll();
        }

        private void setPoruke()
        {
            PorukaDAO poruke = new MySqlPorukaDAO();
            dgvPoruke.DataSource = poruke.dtGetAll();
            dgvPoruke.Columns[4].Visible = false;
            dgvPoruke.Columns[6].Visible = false;
        }
        private void setZadatak()
        {
            ZadatakDAO ZadatakDao = new MySqlZadatakDAO();
            dgvZadatak.DataSource = ZadatakDao.dtGetAll(user.IDOsobe);
            dgvZadatak.Columns[0].Visible = false;
            dgvZadatak.Columns[4].Visible = false;
            dgvZadatak.Columns[5].Visible = false;
        }

        private void dgvPoruke_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPoruke.CurrentCell == null)
            {
                return;
            }
            txtSadrzajPoruke.Text = 
                dgvPoruke.Rows[dgvPoruke.CurrentCell.RowIndex].Cells[4].Value.ToString();
        }

        private void btnSetAsDone_Click(object sender, EventArgs e)
        {
            PorukaDAO pDao = new MySqlPorukaDAO();
            PorukaDTO poruka = new PorukaDTO();
            poruka.Vrijeme = Convert.ToDateTime(
                dgvPoruke.Rows[dgvPoruke.CurrentCell.RowIndex].Cells["Vrijeme"].Value);
            poruka.IDOsobe = Convert.ToInt32(
                dgvPoruke.Rows[dgvPoruke.CurrentCell.RowIndex].Cells["IDOsobe"].Value);
            poruka.KorisnickoIme = Convert.ToString(
                dgvPoruke.Rows[dgvPoruke.CurrentCell.RowIndex].Cells["KorisnickoIme"].Value);

            pDao.setAsDone(poruka);

            setPoruke();
        }

        private void btnNewBook_Click(object sender, EventArgs e)
        {
            newBookForm nBook = new newBookForm();
            nBook.ShowDialog();
            if(nBook.DialogResult == DialogResult.OK)
            {
                setKnjige();
            }
        }

        private void dgvKnjige_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvKnjige.CurrentCell == null)
            {
                return;
            }
            newBookForm nBookForm = new newBookForm(Convert.ToInt32(
                dgvKnjige.Rows[dgvKnjige.CurrentCell.RowIndex].Cells[5].Value
                ));
            
            nBookForm.ShowDialog();
            if (nBookForm.DialogResult == DialogResult.OK ||
                nBookForm.DialogResult == DialogResult.Cancel)
            {
                setKnjige();
            }
        }

        private void btnKorisnici_Click(object sender, EventArgs e)
        {
            KorisniciForm korisniciForm = new KorisniciForm(user.IDOsobe);
            korisniciForm.ShowDialog();
        }

        private void btnNewTask_Click(object sender, EventArgs e)
        {
            ZadatakForm zadatakForm = new ZadatakForm(user.IDOsobe);
            zadatakForm.ShowDialog();
            setZadatak();
        }

        private void dgvZadatak_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvZadatak.CurrentCell == null)
            {
                return;
            }
            int idZad = Convert.ToInt32(
                dgvZadatak.Rows[dgvZadatak.CurrentCell.RowIndex].Cells[0].Value);
            ZadatakDAO zadatakDao = new MySqlZadatakDAO();
            ZadatakDTO zadatakDto = zadatakDao.get(idZad);
            ZadatakForm zadatakForm = new ZadatakForm(user.IDOsobe, zadatakDto);
            zadatakForm.ShowDialog();
            setZadatak();
        }
    }
}
