﻿using bProjektni.data.dao;
using bProjektni.data.dao.MySqlDAO;
using bProjektni.data.dto;
using bProjektni.view_newBook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni
{
    public partial class newBookForm : Form
    {
        private bool newBookMode;
        private int idKnjige;

        public newBookForm()
        {
            InitializeComponent();
            newBookMode = true;
        }

        public newBookForm(int idKnjige) : this()
        {
            this.idKnjige = idKnjige;
            newBookMode = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void newBookForm_Load(object sender, EventArgs e)
        {
            JezikDAO jezikDao = new MySqlJezikDAO();
            cmbJezici.DataSource = jezikDao.dtGetAll();
            cmbJezici.DisplayMember = "Jezik";
            cmbJezici.SelectedItem = null;

            AdminDAO adminDao = new MySqlAdminDAO();
            cmbNadzor.DataSource = adminDao.dtGetAll();
            cmbNadzor.DisplayMember = "KorisnickoIme";

            IzdavacDAO izdavacDao = new MySqlIzdavacDAO();
            cmbIzdavac.DataSource = izdavacDao.dtGetAll();
            cmbIzdavac.DisplayMember = "Naziv";

            /*Same form is used for add and update*/
            if (newBookMode)
            {
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
            }
            else
            {
                btnSave.Visible = false;

                KnjigaDTO knjiga = new KnjigaDTO();
                knjiga.IdKnjige = idKnjige;
                KnjigaDAO knjigaDao = new MySqlKnjigaDAO();
                knjiga = knjigaDao.getKnjiga(knjiga);


                txtNaziv.Text = knjiga.Naziv;
                cmbIzdavac.Text = knjiga.Izdavac;
                cmbJezici.Text = knjiga.Jezik;
                cmbNadzor.Text = knjiga.Nadzor;
                dtpIzdavanja.Value = Convert.ToDateTime(knjiga.DatumIzdavanja).Date;
            }

        }

        private void getKnjiga(NewBook newBook)
        {
            newBook.Knjiga.Naziv = txtNaziv.Text;
            newBook.Knjiga.Izdavac = cmbIzdavac.Text;
            newBook.Knjiga.Jezik = cmbJezici.Text;
            newBook.Knjiga.DatumIzdavanja = dtpIzdavanja.Value.ToString("yyyy-MM-dd");
            newBook.Knjiga.DatumKreiranja = dtpIzdavanja.Value.ToString("yyyy-MM-dd");
            newBook.Knjiga.Nadzor = cmbNadzor.Text;
            newBook.Knjiga.IdKnjige = idKnjige;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            NewBook newBook = new NewBook();
            getKnjiga(newBook);
            
            if(newBook.save() == true)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Došlo je do greške!");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            NewBook newBook = new NewBook();
            getKnjiga(newBook);

            if (newBook.update() == true)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Došlo je do greške!");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            NewBook newBook = new NewBook();
            getKnjiga(newBook);

            if (newBook.delete() == true)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Došlo je do greške!");
            }

        }
    }
}
