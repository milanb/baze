﻿namespace bProjektni
{
    partial class ZadatakForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZadatakForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtOpis = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dpPocetka = new System.Windows.Forms.DateTimePicker();
            this.dpRok = new System.Windows.Forms.DateTimePicker();
            this.chbZavrsen = new System.Windows.Forms.CheckBox();
            this.chbBlokiran = new System.Windows.Forms.CheckBox();
            this.txtBrPredvidjenih = new System.Windows.Forms.TextBox();
            this.txtBrUtrosenih = new System.Windows.Forms.TextBox();
            this.txtBrPotrebnih = new System.Windows.Forms.TextBox();
            this.btnCnacel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Opis";
            // 
            // txtOpis
            // 
            this.txtOpis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOpis.Location = new System.Drawing.Point(115, 194);
            this.txtOpis.Multiline = true;
            this.txtOpis.Name = "txtOpis";
            this.txtOpis.Size = new System.Drawing.Size(293, 152);
            this.txtOpis.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Datum Početka";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Rok";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Broj Predviđenih Sati";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Broj Utrošenih Sati";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Još Potrebno Sati";
            // 
            // dpPocetka
            // 
            this.dpPocetka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dpPocetka.Location = new System.Drawing.Point(114, 13);
            this.dpPocetka.Name = "dpPocetka";
            this.dpPocetka.Size = new System.Drawing.Size(294, 20);
            this.dpPocetka.TabIndex = 1;
            // 
            // dpRok
            // 
            this.dpRok.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dpRok.Location = new System.Drawing.Point(113, 52);
            this.dpRok.Name = "dpRok";
            this.dpRok.Size = new System.Drawing.Size(294, 20);
            this.dpRok.TabIndex = 2;
            // 
            // chbZavrsen
            // 
            this.chbZavrsen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chbZavrsen.AutoSize = true;
            this.chbZavrsen.Location = new System.Drawing.Point(114, 86);
            this.chbZavrsen.Name = "chbZavrsen";
            this.chbZavrsen.Size = new System.Drawing.Size(65, 17);
            this.chbZavrsen.TabIndex = 3;
            this.chbZavrsen.Text = "Završen";
            this.chbZavrsen.UseVisualStyleBackColor = true;
            // 
            // chbBlokiran
            // 
            this.chbBlokiran.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chbBlokiran.AutoSize = true;
            this.chbBlokiran.Location = new System.Drawing.Point(185, 86);
            this.chbBlokiran.Name = "chbBlokiran";
            this.chbBlokiran.Size = new System.Drawing.Size(64, 17);
            this.chbBlokiran.TabIndex = 4;
            this.chbBlokiran.Text = "Blokiran";
            this.chbBlokiran.UseVisualStyleBackColor = true;
            // 
            // txtBrPredvidjenih
            // 
            this.txtBrPredvidjenih.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBrPredvidjenih.Location = new System.Drawing.Point(114, 115);
            this.txtBrPredvidjenih.Name = "txtBrPredvidjenih";
            this.txtBrPredvidjenih.Size = new System.Drawing.Size(293, 20);
            this.txtBrPredvidjenih.TabIndex = 5;
            // 
            // txtBrUtrosenih
            // 
            this.txtBrUtrosenih.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBrUtrosenih.Location = new System.Drawing.Point(115, 142);
            this.txtBrUtrosenih.Name = "txtBrUtrosenih";
            this.txtBrUtrosenih.Size = new System.Drawing.Size(293, 20);
            this.txtBrUtrosenih.TabIndex = 6;
            // 
            // txtBrPotrebnih
            // 
            this.txtBrPotrebnih.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBrPotrebnih.Location = new System.Drawing.Point(115, 168);
            this.txtBrPotrebnih.Name = "txtBrPotrebnih";
            this.txtBrPotrebnih.Size = new System.Drawing.Size(293, 20);
            this.txtBrPotrebnih.TabIndex = 7;
            // 
            // btnCnacel
            // 
            this.btnCnacel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCnacel.Location = new System.Drawing.Point(333, 368);
            this.btnCnacel.MaximumSize = new System.Drawing.Size(75, 23);
            this.btnCnacel.Name = "btnCnacel";
            this.btnCnacel.Size = new System.Drawing.Size(75, 23);
            this.btnCnacel.TabIndex = 10;
            this.btnCnacel.Text = "Cancel";
            this.btnCnacel.UseVisualStyleBackColor = true;
            this.btnCnacel.Click += new System.EventHandler(this.btnCnacel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(252, 368);
            this.btnSave.MaximumSize = new System.Drawing.Size(75, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(171, 368);
            this.btnDelete.MaximumSize = new System.Drawing.Size(75, 23);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ZadatakForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 403);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCnacel);
            this.Controls.Add(this.txtBrPotrebnih);
            this.Controls.Add(this.txtBrUtrosenih);
            this.Controls.Add(this.txtBrPredvidjenih);
            this.Controls.Add(this.chbBlokiran);
            this.Controls.Add(this.chbZavrsen);
            this.Controls.Add(this.dpRok);
            this.Controls.Add(this.dpPocetka);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtOpis);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(436, 442);
            this.Name = "ZadatakForm";
            this.Text = "Zadatak";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOpis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dpPocetka;
        private System.Windows.Forms.DateTimePicker dpRok;
        private System.Windows.Forms.CheckBox chbZavrsen;
        private System.Windows.Forms.CheckBox chbBlokiran;
        private System.Windows.Forms.TextBox txtBrPredvidjenih;
        private System.Windows.Forms.TextBox txtBrUtrosenih;
        private System.Windows.Forms.TextBox txtBrPotrebnih;
        private System.Windows.Forms.Button btnCnacel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
    }
}