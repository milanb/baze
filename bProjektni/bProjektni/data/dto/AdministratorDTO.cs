﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class AdministratorDTO
    {
        private string korisnickoIme;
        private int idOsobe;

        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public int IdOsobe { get => idOsobe; set => idOsobe = value; }
    }
}
