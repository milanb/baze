﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class Aktivni_korisniciDTO
    {


        private string korisnickoIme;
        private string imeIPrezime;
        private string email;
        private string lozinka;
        private DateTime datumRegistracije;

   
        public string ImeIPrezime { get => imeIPrezime; set => imeIPrezime = value; }
        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string Email { get => email; set => email = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }
        public DateTime DatumRegistracije { get => datumRegistracije; set => datumRegistracije = value; }
    }
}
