﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class ZadatakDTO
    {
        private string opis;
        private DateTime datumPocetka;
        private DateTime rok;
        private bool zavrsen;
        private bool blokiran;
        private int brPredvidjenihH;
        private int brUtrosenihH;
        private int brPotebnihH;
        private int idZadatka;

        public string Opis { get => opis; set => opis = value; }
        public DateTime DatumPocetka { get => datumPocetka; set => datumPocetka = value; }
        public DateTime Rok { get => rok; set => rok = value; }
        public bool Zavrsen { get => zavrsen; set => zavrsen = value; }
        public bool Blokiran { get => blokiran; set => blokiran = value; }
        public int BrPredvidjenihH { get => brPredvidjenihH; set => brPredvidjenihH = value; }
        public int BrUtrosenihH { get => brUtrosenihH; set => brUtrosenihH = value; }
        public int BrPotebnihH { get => brPotebnihH; set => brPotebnihH = value; }
        public int IdZadatka { get => idZadatka; set => idZadatka = value; }
    }
}
