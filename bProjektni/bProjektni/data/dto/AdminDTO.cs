﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class AdminDTO
    {
        private string korisnickoIme;
        private string imeIPrezime;
        private string lozinka;
        private int iDOsobe;

        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string ImeIPrezime { get => imeIPrezime; set => imeIPrezime = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }
        public int IDOsobe { get => iDOsobe; set => iDOsobe = value; }
    }
}
