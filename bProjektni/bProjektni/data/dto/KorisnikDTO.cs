﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class KorisnikDTO
    {
        private string korisnickoIme;
        private string lozinka;
        private bool aktivan;
        private int idOsobe;

        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }
        public bool Aktivan { get => aktivan; set => aktivan = value; }
        public int IdOsobe { get => idOsobe; set => idOsobe = value; }
    }
}
