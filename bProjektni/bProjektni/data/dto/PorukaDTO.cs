﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class PorukaDTO
    {
        private string imeIPrezime;
        private string korisnickoIme;
        private string knjiga;
        private DateTime vrijeme;
        private string sadrzaj;
        private string email;
        private int iDOsobe;

        public string ImeIPrezime { get => imeIPrezime; set => imeIPrezime = value; }
        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string Knjiga { get => knjiga; set => knjiga = value; }
        public DateTime Vrijeme { get => vrijeme; set => vrijeme = value; }
        public string Sadrzaj { get => sadrzaj; set => sadrzaj = value; }
        public string Email { get => email; set => email = value; }
        public int IDOsobe { get => iDOsobe; set => iDOsobe = value; }
    }
}
