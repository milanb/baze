﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class TelefonDTO
    {
        private string brojTelefona;
        private int idOsobe;

        public string BrojTelefona { get => brojTelefona; set => brojTelefona = value; }
        public int IdOsobe { get => idOsobe; set => idOsobe = value; }
    }
}
