﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class OsobaDTO
    {
        private int idOsobe;
        private string imeIPrezime;
        private string email;

        public int IdOsobe { get => idOsobe; set => idOsobe = value; }
        public string ImeIPrezime { get => imeIPrezime; set => imeIPrezime = value; }
        public string Email { get => email; set => email = value; }
    }
}
