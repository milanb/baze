﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class KnjigaDTO
    {
        private string naziv;
        private string jezik;
        private string izdavac;
        private string nadzor;
        private int nadzorId;
        private int brPosjeta;
        private int idKnjige;
        private string datumIzdavanja;
        private string datumKreiranja;


        public string Naziv { get => naziv; set => naziv = value; }
        public string Jezik { get => jezik; set => jezik = value; }
        public string Izdavac { get => izdavac; set => izdavac = value; }
        public string Nadzor { get => nadzor; set => nadzor = value; }
        public int BrPosjeta { get => brPosjeta; set => brPosjeta = value; }
        public int IdKnjige { get => idKnjige; set => idKnjige = value; }
        public string DatumIzdavanja { get => datumIzdavanja; set => datumIzdavanja = value; }
        public string DatumKreiranja { get => datumKreiranja; set => datumKreiranja = value; }
        public int NadzorId { get => nadzorId; set => nadzorId = value; }
    }
}
