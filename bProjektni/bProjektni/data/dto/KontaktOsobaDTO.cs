﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dto
{
    public class KontaktOsobaDTO
    {
        private string adresa;
        private int idOsobe;
        private DataTable telefon;

        public string Adresa { get => adresa; set => adresa = value; }
        public int IdOsobe { get => idOsobe; set => idOsobe = value; }
        public DataTable Telefon { get => telefon; set => telefon = value; }
    }
}
