﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface ZadatakDAO
    {
        DataTable dtGetAll(int idOsobe);
        ZadatakDTO get(int idZadatka);
        bool add(ZadatakDTO zadatakDto, int idOsobe);
        bool update(ZadatakDTO zadatakDto);
        bool delete(int idZadatka);

    }
}
