﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface PorukaDAO
    {
        List<PorukaDTO> getAll();
        DataTable dtGetAll();

        bool setAsDone(PorukaDTO poruka);
    }
}
