﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;
using System.Data;

namespace bProjektni.data.dao
{
    public interface Aktivni_korisniciDAO
    {
        List<Aktivni_korisniciDTO> getUsers();
        DataTable dtGetAll();

        List<Aktivni_korisniciDTO> getUsers(Aktivni_korisniciDTO korisnik);

        int insert(Aktivni_korisniciDTO korisnik);
    }
}
