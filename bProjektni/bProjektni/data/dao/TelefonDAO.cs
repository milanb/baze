﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface TelefonDAO
    {
        DataTable dtGetAll(int idOsobe);
        bool add(TelefonDTO telefon);
    }
}
