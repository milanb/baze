﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface KontaktOsobaDAO
    {
        bool isKontaktOsoba(int idOsobe);
        KontaktOsobaDTO getKO(int idOsobe);
        bool add(KontaktOsobaDTO kontaktOsoba);
        bool update(KontaktOsobaDTO osoba);
    }
}
