﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    interface AdministratorDAO
    {
        Boolean add(AdministratorDTO admin);
        bool update(AdministratorDTO admin);
    }
}
