﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface KorisnikDAO
    {
        bool isKorisnik(int idOsobe);
        KorisnikDTO getKorisnik(int idOsobe);
        bool add(KorisnikDTO korisnik);
        bool update(KorisnikDTO korisnik);
        bool updateLozinka(KorisnikDTO korisnik);

    }
}
