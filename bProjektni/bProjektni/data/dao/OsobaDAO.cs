﻿using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao
{
    public interface OsobaDAO
    {
        DataTable dtGetAll();
        int add(OsobaDTO osoba);
        bool delete(int idOsobe);
        Boolean update(OsobaDTO osoba);
    }
}
