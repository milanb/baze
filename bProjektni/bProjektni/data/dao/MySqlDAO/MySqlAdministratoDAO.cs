﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlAdministratoDAO : AdministratorDAO
    {
        private string qAdd = "INSERT INTO `administrator` (`KorisnickoIme`, `IDOsobe`)"+
            "VALUES(@korisnickoIme, @idOsobe);";
        private string qUpdate = "";

        public Boolean add(AdministratorDTO admin)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("korisnickoIme", admin.KorisnickoIme));
            parametri.Add(new MySqlParameter("idOsobe", admin.IdOsobe));

            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD ADMINISTRATOR! \n" + e.Message);
                return false;
            }

            return true;
        }

        public bool update(AdministratorDTO admin)
        {
            return true;
        }
    }

}
