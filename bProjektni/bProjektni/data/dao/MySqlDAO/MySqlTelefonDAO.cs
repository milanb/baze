﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlTelefonDAO : TelefonDAO
    {
        private string qGetAll = "SELECT * FROM telefon_osobe WHERE IDOsobe = @id";
        private string qAdd = "INSERT INTO `telefon_osobe` " +
            "(`BrojTelefona`, `TipTelefona`,  `IDOsobe`) " +
            "VALUES (@brojTelefona, 'tip tel',  @idOsobe);";


        public bool add(TelefonDTO telefon)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("brojTelefona", telefon.BrojTelefona));
            parametri.Add(new MySqlParameter("idOsobe", telefon.IdOsobe));

            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD TELEFON! \n" + e.Message);
                return false;
            }

            return true;
        }

        public DataTable dtGetAll(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", idOsobe));
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                return null;
            }
            return set.Tables[0];
        }

    }
}
