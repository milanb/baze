﻿using System;
using System.Collections.Generic;
using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlAktivni_korisniciDAO : Aktivni_korisniciDAO
    {
        private string qGetAll = "SELECT * FROM `aktivni_korisnici`";
        private string qGetUsers = "SELECT * FROM `aktivni_korisnici` WHERE " +
            "(KorisnickoIme = @ki OR @ki IS NULL) " +
            "AND (ImeIPrezime = @ip OR @ip IS NULL) " +
            "AND (Email = @em OR @em IS NULL)" +
            "AND (LozinkaZaPristupSistemu = @lo OR @lo IS NULL) " +
            "AND (DatumRegistracije = @dr OR @dr IS NULL)";
        private string qInsert = "";

        private Aktivni_korisniciDTO dataRowToAktivni_korisnik(DataRow dr)
        {
            Aktivni_korisniciDTO newAK = new Aktivni_korisniciDTO();
            try
            {
                newAK.KorisnickoIme = Convert.ToString(dr["KorisnickoIme"]);
                newAK.ImeIPrezime = Convert.ToString(dr["ImeIPrezime"]);
                newAK.Email = Convert.ToString(dr["Email"]);
                newAK.Lozinka = Convert.ToString(dr["LozinkaZaPristupSistemu"]);
                newAK.DatumRegistracije = Convert.ToDateTime(dr["DatumRegistracije"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newAK;
        }

        public DataTable dtGetAll()
        {
            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll
                );
            return set.Tables[0];
        }

        public List<Aktivni_korisniciDTO> getUsers()
        {
            List<Aktivni_korisniciDTO> akKorisnici = new List<Aktivni_korisniciDTO>();
            
            foreach(DataRow dr in dtGetAll().Rows)
            {
                akKorisnici.Add(dataRowToAktivni_korisnik(dr));
            }
            return akKorisnici;    
        }


        public DataTable dtGetUsers(Aktivni_korisniciDTO user)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("ki", user.KorisnickoIme));
            parametri.Add(new MySqlParameter("ip", user.ImeIPrezime));
            parametri.Add(new MySqlParameter("em", user.Email));
            parametri.Add(new MySqlParameter("lo", user.Lozinka));
            parametri.Add(new MySqlParameter("dr", null));//user.DatumRegistracije


            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetUsers,
                parametri.ToArray()
                );

            return set.Tables[0];
        }

        public List<Aktivni_korisniciDTO> getUsers(Aktivni_korisniciDTO user)
        {
            List<Aktivni_korisniciDTO> akKorisnici = new List<Aktivni_korisniciDTO>();

            foreach (DataRow dr in dtGetUsers(user).Rows)
            {
                akKorisnici.Add(dataRowToAktivni_korisnik(dr));
            }
            return akKorisnici;
        }


        public int insert(Aktivni_korisniciDTO korisnik)
        {
            throw new NotImplementedException();
        }
    }
}