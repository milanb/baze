﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    
    public class MySqlKorisnikDAO : KorisnikDAO
    {
        private string qIsKorisnik = "SELECT * FROM korisnik WHERE " +
            "`IDOsobe` = @id";
        private string qAdd = "INSERT INTO `korisnik` " +
            "(`KorisnickoIme`, `LozinkaZaPristupSistemu`, `IDOsobe`) " +
            "VALUES (@korisnickoIme, @lozinka, @idOsobe);";
        private string qUpdate = "UPDATE `korisnik` SET " +
            "`KorisnickoIme`=@korisnickoIme" +
            " WHERE `IDOsobe`=@idOsobe";
        private string qUpdatePass = "UPDATE `korisnik` SET " +
            "`LozinkaZaPristupSistemu`=@lozinka" +
            " WHERE `IDOsobe`=@idOsobe";

        private KorisnikDTO dataRowToKorisnik(DataRow dr)
        {
            KorisnikDTO newKorisnik = new KorisnikDTO();
            try
            {
                newKorisnik.KorisnickoIme = Convert.ToString(dr["KorisnickoIme"]);
                newKorisnik.Aktivan = Convert.ToBoolean(dr["Aktivan"]);
                newKorisnik.Lozinka = Convert.ToString(dr["LozinkaZaPristupSistemu"]);

            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newKorisnik;
        }

        public KorisnikDTO getKorisnik(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", idOsobe));
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qIsKorisnik,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                return null;
            }
            if(set.Tables[0].Rows[0] != null)
            {
                return dataRowToKorisnik(set.Tables[0].Rows[0]);
            }
            return null;
        }

        public bool isKorisnik(int id)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", id));
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qIsKorisnik,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                return false;
            }

            if (set.Tables[0].Rows.Count < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool add(KorisnikDTO korisnik)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("korisnickoIme", korisnik.KorisnickoIme));
            parametri.Add(new MySqlParameter("lozinka", korisnik.Lozinka));
            parametri.Add(new MySqlParameter("idOsobe", korisnik.IdOsobe));

            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD Korisnik! \n" + e.Message);
                return false;
            }
            return true;
        }

        public bool update(KorisnikDTO korisnik)
        {
            
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", korisnik.IdOsobe));
            parametri.Add(new MySqlParameter("korisnickoIme", korisnik.KorisnickoIme));
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdate,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska UPDATE Korisnika! \n" + e.InnerException.Message);
                return false;
            }
            return true;
            
        }
        public bool updateLozinka(KorisnikDTO korisnik)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", korisnik.IdOsobe));
            parametri.Add(new MySqlParameter("lozinka", korisnik.Lozinka));
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdatePass,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska UPDATE LOZNIKA! \n" + e.Message);
                return false;
            }
            return true;
        }
    }
}
