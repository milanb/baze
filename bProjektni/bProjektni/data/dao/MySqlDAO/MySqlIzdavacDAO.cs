﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao.MySqlDAO
{
    class MySqlIzdavacDAO : IzdavacDAO
    {
        private string qGetAll = "SELECT * FROM `izdavac`";
        public DataTable dtGetAll()
        {
            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll
                );

            return set.Tables[0];
        }
    }
}
