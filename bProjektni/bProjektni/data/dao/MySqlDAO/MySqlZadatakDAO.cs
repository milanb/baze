﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlZadatakDAO : ZadatakDAO
    {
        private string qGetAll = "SELECT * FROM `zadatak` WHERE " +
            "`IDZadatka` IN (SELECT je_zaduzen_za.IDZadatka FROM " +
            "je_zaduzen_za WHERE je_zaduzen_za.IDOsobe = @idOsobe) AND " +
            "`Zavrsen` = 0";
        private string qGetZadatak = "SELECT * FROM zadatak WHERE IDZadatka = " +
            "@idZadatka";
        private string qAdd = "INSERT INTO `zadatak` (`IDZadatka`, `Opis`, " +
            "`DatumPocetka`, `Rok`, `Zavrsen`, `Blokiran`, " +
            "`BrojPredvidjenihSati`, `BrojUtrosenihSati`, `JosPotrebnihSati`) VALUES " +
            "(NULL, @opis, @datumPocetka, @rok, @zavrsen, @blokiran, " +
            "@brPredvidjenih, @brUtrosenih, @brPotrebnih);";
        private string qUpdate = "UPDATE `zadatak` SET `Opis` = @opis," +
            "`DatumPocetka` = @datumPocetka,`Rok` = @rok,`Zavrsen` = @zavrsen," +
            "`Blokiran` = @blokiran,`BrojPredvidjenihSati` = @brPredvidjenih," +
            "`BrojUtrosenihSati` = @brUtrosenih,`JosPotrebnihSati` = @brPotrebnih " +
            "WHERE `IDZadatka` = @idZadatka";
        private string qZaduzenje = "INSERT INTO `je_zaduzen_za`(`TipZaduzenja`, " +
            "`KorisnickoIme`, `IDOsobe`, `IDZadatka`) VALUES " +
            "('Admin',@korisnickoIme,@idOsobe,@idZadatka)";
        private string qGetId = "SELECT LAST_INSERT_ID() FROM zadatak";
        private string qDelete = "DELETE FROM `zadatak` WHERE " +
            "`IDZadatka` = @idZadatka;";

        private ZadatakDTO dataRowToZadatak(DataRow dr)
        {

            ZadatakDTO newZadatak = new ZadatakDTO();
            try
            {
                newZadatak.IdZadatka = Convert.ToInt32(dr["IDZadatka"]);
                newZadatak.Opis = Convert.ToString(dr["Opis"]);
                newZadatak.DatumPocetka = Convert.ToDateTime(dr["DatumPocetka"]);
                newZadatak.Rok = Convert.ToDateTime(dr["Rok"]);
                newZadatak.Zavrsen = Convert.ToBoolean(dr["Zavrsen"]);
                newZadatak.Blokiran = Convert.ToBoolean(dr["Blokiran"]);
                newZadatak.BrPredvidjenihH = Convert.ToInt32(dr["BrojPredvidjenihSati"]);
                newZadatak.BrUtrosenihH = Convert.ToInt32(dr["BrojUtrosenihSati"]);
                newZadatak.BrPotebnihH = Convert.ToInt32(dr["JosPotrebnihSati"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newZadatak;
        }

        public DataTable dtGetAll(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", idOsobe));

            DataSet set = MySqlHelper.ExecuteDataset(
            bProjektni.Properties.Resources.ConnectionString,
            qGetAll,
            parametri.ToArray()
            );

            return set.Tables[0];
        }

        public ZadatakDTO get(int idZadataka)
        {
            ZadatakDTO retZadatak = null;
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idZadatka", idZadataka));

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetZadatak,
                parametri.ToArray()
                );

                retZadatak = dataRowToZadatak(set.Tables[0].Rows[0]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska GET ZADATAK! \n" + e.Message);
            }
            return retZadatak;
        }

        public bool add(ZadatakDTO zadatakDto, int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("opis", zadatakDto.Opis));
            parametri.Add(new MySqlParameter("datumPocetka",
                zadatakDto.DatumPocetka.ToString("yyyy-MM-dd")));
            parametri.Add(new MySqlParameter("rok", 
                zadatakDto.Rok.ToString("yyyy-MM-dd")));
            parametri.Add(new MySqlParameter("zavrsen", zadatakDto.Zavrsen));
            parametri.Add(new MySqlParameter("blokiran", zadatakDto.Blokiran));
            parametri.Add(new MySqlParameter("brPredvidjenih", zadatakDto.BrPredvidjenihH));
            parametri.Add(new MySqlParameter("brUtrosenih", zadatakDto.BrUtrosenihH));
            parametri.Add(new MySqlParameter("brPotrebnih", zadatakDto.BrPotebnihH));

            List<MySqlParameter> pomParametri = new List<MySqlParameter>();
            pomParametri.Add(new MySqlParameter("idOsobe",idOsobe));
            KorisnikDTO korisnikDto = null;

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );

                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetId,
                parametri.ToArray()
                );
                //idZadatka
                zadatakDto.IdZadatka = Convert.ToInt32(set.Tables[0].Rows[0][0]);

                KorisnikDAO korisnikDao = new MySqlKorisnikDAO();
                //korisnickoIme 
                korisnikDto = korisnikDao.getKorisnik(idOsobe);

                pomParametri.Add(new MySqlParameter("korisnickoIme", korisnikDto.KorisnickoIme));
                pomParametri.Add(new MySqlParameter("idZadatka", zadatakDto.IdZadatka));

                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qZaduzenje,
                pomParametri.ToArray()
                );

            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD ZADATAK! \n" + e.InnerException.Message);
                return false;
            }
            return true;
        }

        public bool update(ZadatakDTO zadatakDto)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idZadatka", zadatakDto.IdZadatka));
            parametri.Add(new MySqlParameter("opis", zadatakDto.Opis));
            parametri.Add(new MySqlParameter("datumPocetka", zadatakDto.DatumPocetka));
            parametri.Add(new MySqlParameter("rok", zadatakDto.Rok));
            parametri.Add(new MySqlParameter("zavrsen", zadatakDto.Zavrsen));
            parametri.Add(new MySqlParameter("blokiran", zadatakDto.Blokiran));
            parametri.Add(new MySqlParameter("brPredvidjenih", zadatakDto.BrPredvidjenihH));
            parametri.Add(new MySqlParameter("brUtrosenih", zadatakDto.BrUtrosenihH));
            parametri.Add(new MySqlParameter("brPotrebnih", zadatakDto.BrPotebnihH));

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdate,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska UPDATE ZADATAK! \n" + e.InnerException.Message);
                return false;
            }
            return true;
        }

        public bool delete(int idZadatka)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idZadatka", idZadatka));
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qDelete,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska DELETE ZADATAK! \n" + e.Message);
                return false;
            }
            return true;
        }
    }
}
