﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    class MySqlKnjigaDAO : KnjigaDAO
    {
        private string qGetAll = "SELECT DISTINCT K.Naziv, `Jezik`," +
            "`BrojPosjeta`, I.Naziv AS Izdavac , " +
            "`ImeIPrezime` AS Nadzor,IDKnjige,DatumIzdavanja FROM knjiga K " +
            "INNER JOIN osoba O ON K.Nadzor_IDOsobe = O.IDOsobe " +
            "INNER JOIN izdavac I ON K.FK_Izdavac = I.Naziv " +
            "INNER JOIN jezik J ON K.FK_Jezik = J.Jezik WHERE K.IDKnjige > 0";

        private string qGetKnjiga = "SELECT Naziv, FK_Jezik AS Jezik, " +
            "DatumIzdavanja, FK_Izdavac AS Izdavac, `Nadzor_KorisnickoIme(FK)` " +
            "AS Nadzor, IDKnjige, BrojPosjeta FROM knjiga WHERE IDKnjige = @id";

        private string qAdd = "INSERT INTO `knjiga` " +
            "(`BrojPosjeta`, `IDKnjige`, `Naziv`, `FK_Jezik`, `DatumIzdavanja`, " +
            "`DatumKreiranja`, `DatumObjave`, `IDZadatka`, `FK_Izdavac`, " +
            "`FK_Komplet`, `Nadzor_KorisnickoIme(FK)`, `Nadzor_IDOsobe`, " +
            "`OdgovornaO_KorisnickoIme(FK)`, `OdgovornaO_IDOsobe`) " +

            "VALUES ('0', NULL, @naziv, @jezik, @datum, " +
            "@datum, NULL, NULL, @izdavac, NULL, " +
            "@korisnickoIme, @IDOsobe, @korisnickoIme, @IDOsobe);";

        private string qUpdate = "UPDATE `knjiga` SET `Naziv` = @na, " +
            "`FK_Jezik` = @je, `DatumIzdavanja` = @di, `FK_Izdavac` = @iz," +
            "`Nadzor_KorisnickoIme(FK)` = @nki, `Nadzor_IDOsobe` = @nid " +
            "WHERE `knjiga`.`IDKnjige` = @id;";

        private string qDelete = "DELETE FROM `knjiga` WHERE IDKnjige = @id";

        private KnjigaDTO dataRowToKnjiga(DataRow dr)
        {
            KnjigaDTO newKnjiga = new KnjigaDTO();
            try
            {
                newKnjiga.Naziv = Convert.ToString(dr["Naziv"]);
                newKnjiga.Jezik = Convert.ToString(dr["Jezik"]);
                newKnjiga.Izdavac = Convert.ToString(dr["Izdavac"]);
                newKnjiga.Nadzor = Convert.ToString(dr["Nadzor"]);
                newKnjiga.IdKnjige = Convert.ToInt32(dr["IDKnjige"]);
                newKnjiga.BrPosjeta = Convert.ToInt32(dr["BrojPosjeta"]);
                newKnjiga.DatumIzdavanja = Convert.ToString(dr["DatumIzdavanja"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newKnjiga;
        }


        public DataTable dtGetAll()
        {
            DataSet set = MySqlHelper.ExecuteDataset(
            bProjektni.Properties.Resources.ConnectionString,
            qGetAll
            );

            return set.Tables[0];
        }

        public Boolean add(KnjigaDTO knjigaDto, AdminDTO adminDto)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("naziv", knjigaDto.Naziv));
            parametri.Add(new MySqlParameter("jezik", knjigaDto.Jezik));
            parametri.Add(new MySqlParameter("izdavac", knjigaDto.Izdavac));
            parametri.Add(new MySqlParameter("datum", knjigaDto.DatumIzdavanja));
            parametri.Add(new MySqlParameter("korisnickoIme", adminDto.KorisnickoIme));
            parametri.Add(new MySqlParameter("IDOsobe", adminDto.IDOsobe));

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );
            }
            catch(Exception e)
            {
                return false;
            }

            return true;
        }

        public KnjigaDTO getKnjiga(KnjigaDTO knjiga)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", knjiga.IdKnjige));

            KnjigaDTO retKnjiga = new KnjigaDTO();

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetKnjiga,
                parametri.ToArray()
                );

                retKnjiga = dataRowToKnjiga(set.Tables[0].Rows[0]);
            }
            catch (Exception e)
            {
            }
            return retKnjiga;
        }

        public bool update(KnjigaDTO knjigaDto )
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", knjigaDto.IdKnjige));
            parametri.Add(new MySqlParameter("na", knjigaDto.Naziv));
            parametri.Add(new MySqlParameter("je", knjigaDto.Jezik));
            parametri.Add(new MySqlParameter("iz", knjigaDto.Izdavac));
            parametri.Add(new MySqlParameter("di", knjigaDto.DatumIzdavanja));
            parametri.Add(new MySqlParameter("nki", knjigaDto.Nadzor));
            parametri.Add(new MySqlParameter("nid", knjigaDto.NadzorId));

            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdate,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska u UPDATE! \n" + e.InnerException.Message);
                return false;
            }
            return true;
        }

        public bool delete(KnjigaDTO knjiga)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", knjiga.IdKnjige));
            
            try
            {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qDelete,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska u DELETE! \n" + e.InnerException.Message);
                return false;
            }
            return true;
        }
    }
}
