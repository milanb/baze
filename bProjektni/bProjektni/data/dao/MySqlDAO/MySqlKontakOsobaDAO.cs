﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlKontakOsobaDAO : KontaktOsobaDAO
    {
        private string qIsKO = "SELECT * FROM kontakt_osoba WHERE " +
            "`IDOsobe` = @id";
        private string qAdd = "INSERT INTO `kontakt_osoba` (`Adresa`, `IDOsobe`)" +
            " VALUES (@adresa, @idOsobe);";
        private string qUpdate = "UPDATE `kontakt_osoba` SET " +
            "`Adresa`= @adresa WHERE `IDOsobe`= @idOsobe " ;

        private KontaktOsobaDTO dataRowToKO(DataRow dr)
        {
            KontaktOsobaDTO newKO = new KontaktOsobaDTO();
            try
            {
                newKO.Adresa = Convert.ToString(dr["Adresa"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newKO;
        }

        public bool isKontaktOsoba(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", idOsobe));
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qIsKO,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                return false;
            }

            if(set.Tables[0].Rows.Count < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


  

        public KontaktOsobaDTO getKO(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("id", idOsobe));
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qIsKO,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                return null;
            }
            KontaktOsobaDTO kO = dataRowToKO(set.Tables[0].Rows[0]);
            TelefonDAO telefonDao = new MySqlTelefonDAO();
            kO.Telefon = telefonDao.dtGetAll(idOsobe);
            return kO;
        }

        public bool add(KontaktOsobaDTO kontaktOsoba)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("adresa", kontaktOsoba.Adresa));
            parametri.Add(new MySqlParameter("idOsobe", kontaktOsoba.IdOsobe));

            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD KONTAKT OSOBA! \n" + e.Message);
                return false;
            }

            return true;
        }

        public bool update(KontaktOsobaDTO osoba)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", osoba.IdOsobe));
            parametri.Add(new MySqlParameter("adresa", osoba.Adresa));
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdate,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska UPDATE KO! \n" + e.Message);
                return false;
            }
            return true;
        }
    }
}
