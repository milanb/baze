﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlJezikDAO : JezikDAO
    {
        private string qGetAll = "SELECT * FROM Jezik";
        private string qGet = "SELECT * FROM Jezik WHERE Jezik = @je";
        private string qAdd = "INSERT INTO `jezik` (`Jezik`) VALUES (@je);";

        public DataTable dtGetAll()
        {
            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll
                );

            return set.Tables[0];
        }

        public DataTable dtGetAll(JezikDTO jezik)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("je", jezik.Jezik));

            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGet,
                parametri.ToArray()
                );

            return set.Tables[0];
        }

        public void add(JezikDTO jezik)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("je", jezik.Jezik));

            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );

        }

        public void setJezik(string jezik)
        {
            JezikDTO jezikDto = new JezikDTO();
            jezikDto.Jezik = jezik;
            DataTable table = dtGetAll(jezikDto);
            if (table.Rows.Count < 1)
            {
                add(jezikDto);
            }
        }
    }
}
