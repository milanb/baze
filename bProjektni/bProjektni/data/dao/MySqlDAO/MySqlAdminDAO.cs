﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlAdminDAO : AdminDAO
    {
        private string qGetAdmins = "SELECT * FROM `Admin` WHERE " +
            "(KorisnickoIme = @ki OR @ki IS NULL) " +
            "AND (ImeIPrezime = @ip OR @ip IS NULL) " +
            "AND (LozinkaZaPristupSistemu = @lo OR @lo IS NULL) " +
            "AND (IDOsobe = @id OR @id = 0)";

        private AdminDTO dataRowToAdmin(DataRow dr)
        {
            AdminDTO newAK = new AdminDTO();
            try
            {
                newAK.KorisnickoIme = Convert.ToString(dr["KorisnickoIme"]);
                newAK.ImeIPrezime = Convert.ToString(dr["ImeIPrezime"]);
                newAK.Lozinka = Convert.ToString(dr["LozinkaZaPristupSistemu"]);
                newAK.IDOsobe= Convert.ToInt32(dr["IDOsobe"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newAK;
        }

        public DataTable dtGetAdmins(AdminDTO user)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("ki", user.KorisnickoIme));
            parametri.Add(new MySqlParameter("ip", user.ImeIPrezime));
            parametri.Add(new MySqlParameter("lo", user.Lozinka));
            parametri.Add(new MySqlParameter("id", user.IDOsobe));

            DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAdmins,
                parametri.ToArray()
                );

            return set.Tables[0];
        }

        public DataTable dtGetAll()
        {
            AdminDTO user = new AdminDTO();
            return dtGetAdmins(user);
        }

        public List<AdminDTO> getAdmins(AdminDTO user)
        {
            List<AdminDTO> admins = new List<AdminDTO>();

            foreach (DataRow dr in dtGetAdmins(user).Rows)
            {
                admins.Add(dataRowToAdmin(dr));
            }
            return admins;
        }

      
    }
}
