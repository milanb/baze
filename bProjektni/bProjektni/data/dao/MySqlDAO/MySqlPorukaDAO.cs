﻿using bProjektni.data.dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlPorukaDAO : PorukaDAO
    {
        private string qGetAll = "SELECT * FROM `messages`";
        private string qDone = "UPDATE `poruka` SET `Odgovorena` = '1' WHERE " +
            " `poruka`.`Vrijeme` = @vr AND" +
            " `poruka`.`KorisnickoIme` = @ki AND" +
            " `poruka`.`IDOsobe` = @id;";

        private PorukaDTO dataRowToMessage(DataRow dr)
        {
            PorukaDTO newMessage = new PorukaDTO();
            try
            {
                newMessage.KorisnickoIme = Convert.ToString(dr["KorisnickoIme"]);
                newMessage.ImeIPrezime = Convert.ToString(dr["ImeIPrezime"]);
                newMessage.Vrijeme = Convert.ToDateTime(dr["Vrijeme"]);
                newMessage.Knjiga = Convert.ToString(dr["Knjiga"]);
                newMessage.Sadrzaj = Convert.ToString(dr["Sadrzaj"]);
                newMessage.Email = Convert.ToString(dr["Email"]);
                newMessage.IDOsobe = Convert.ToInt32(dr["IDOsobe"]);
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska pri parsiranju podataka! \n" + e.Message);
            }
            return newMessage;
        }

        public DataTable dtGetAll()
        {
                DataSet set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll
                );

            return set.Tables[0];
        }

        public List<PorukaDTO> getAll()
        {
            List<PorukaDTO> admins = new List<PorukaDTO>();

            foreach (DataRow dr in dtGetAll().Rows)
            {
                admins.Add(dataRowToMessage(dr));
            }
            return admins;
        }

        public bool setAsDone(PorukaDTO poruka)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("vr", poruka.Vrijeme));
            parametri.Add(new MySqlParameter("ki", poruka.KorisnickoIme));
            parametri.Add(new MySqlParameter("id", poruka.IDOsobe));


            MySqlHelper.ExecuteDataRow(
                bProjektni.Properties.Resources.ConnectionString,
                qDone,
                parametri.ToArray()
                );

            return true;
        }
    }
}
