﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;
using System.Windows.Forms;

namespace bProjektni.data.dao.MySqlDAO
{
    public class MySqlOsobaDAO : OsobaDAO
    {
        private string qGetAll = "SELECT * FROM osoba";
        private string qAdd = "INSERT INTO `osoba`(`imeIPrezime`, `Email`)" +
            " VALUES (@imeIPrezime,@email)";
        private string qGetId = "SELECT LAST_INSERT_ID() FROM osoba";
        private string qDelete = "DELETE FROM `osoba` WHERE " +
            "`osoba`.`IDOsobe` = @idOsobe;";
        private string qUpdate = "UPDATE `osoba` SET " +
            "`ImeIPrezime`= @imeIPrezime,`Email`= @email WHERE `osoba`.`IDOsobe` = '@idOsobe'";


        public int add(OsobaDTO osobaDto)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("imeIPrezime", osobaDto.ImeIPrezime));
            parametri.Add(new MySqlParameter("email", osobaDto.Email));

            DataSet set;
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qAdd,
                parametri.ToArray()
                );

                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetId,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska ADD OSOBA! \n" + e.Message);
                return 0;
            }
            int retVal = Convert.ToInt32(set.Tables[0].Rows[0][0]);
            return retVal;
        }

        public DataTable dtGetAll()
        {
            DataSet set;
            try
            {
                set = MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qGetAll
                );
            }
            catch (Exception e)
            {
                return null;
            }

            return set.Tables[0];
        }

        public bool delete(int idOsobe)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", idOsobe));
            try
            {
                MySqlHelper.ExecuteDataset(
                bProjektni.Properties.Resources.ConnectionString,
                qDelete,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska DELETE OSOBA! \n" + e.Message);
                return false;
            }
            return true;
        }

        public bool update(OsobaDTO osoba)
        {
            List<MySqlParameter> parametri = new List<MySqlParameter>();
            parametri.Add(new MySqlParameter("idOsobe", osoba.IdOsobe));
            parametri.Add(new MySqlParameter("imeIPrezime", osoba.ImeIPrezime));
            parametri.Add(new MySqlParameter("email", osoba.Email));
            try
            {
                MySqlHelper.ExecuteDataRow(
                bProjektni.Properties.Resources.ConnectionString,
                qUpdate,
                parametri.ToArray()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show("Greska UPDATE OSOBA! \n" + e.Message);
                return false;
            }
            return true;
        }
    }
}
