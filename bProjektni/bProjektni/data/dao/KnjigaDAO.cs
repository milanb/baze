﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bProjektni.data.dto;

namespace bProjektni.data.dao
{
    public interface KnjigaDAO
    {
        DataTable dtGetAll();
        Boolean add(KnjigaDTO knjiga, AdminDTO adminDto);
        KnjigaDTO getKnjiga(KnjigaDTO knjiga);
        Boolean update(KnjigaDTO knjigaDto);
        bool delete(KnjigaDTO knjiga);
    }
}
