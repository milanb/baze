﻿using bProjektni.data.dao;
using bProjektni.data.dao.MySqlDAO;
using bProjektni.data.dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bProjektni.view_newBook
{
    
    public class NewBook
    {
        private KnjigaDTO knjiga;
        public KnjigaDTO Knjiga { get => knjiga; set => knjiga = value; }

        public NewBook()
        {
            knjiga = new KnjigaDTO();
        }
        

        private AdminDTO getAdmin(AdminDTO adminDto)
        {
            adminDto.KorisnickoIme = knjiga.Nadzor;
            AdminDAO adminDao = new MySqlAdminDAO();

            List<AdminDTO> admins = adminDao.getAdmins(adminDto);
            return admins[0];
        }


        internal Boolean save()
        {
            /*data preparation*/
            AdminDTO adminDto = new AdminDTO();
            adminDto = getAdmin(adminDto);
            
            if (adminDto == null)
            {
                return false;
            }

            JezikDAO JezikDao = new MySqlJezikDAO();
            JezikDao.setJezik(knjiga.Jezik);

            /*add Book*/
            KnjigaDAO knjigaDao = new MySqlKnjigaDAO();
            knjigaDao.add(knjiga, adminDto);

            return true;
        }

        internal bool update()
        {
            /*data preparation*/
            AdminDTO adminDto = new AdminDTO();
            adminDto = getAdmin(adminDto);
            knjiga.NadzorId = adminDto.IDOsobe;

            JezikDAO JezikDao = new MySqlJezikDAO();
            JezikDao.setJezik(knjiga.Jezik);

            /*update*/
            KnjigaDAO knjigaDao = new MySqlKnjigaDAO();
            return knjigaDao.update(knjiga);
        }

        internal Boolean delete()
        {
            KnjigaDAO knjigaDao = new MySqlKnjigaDAO();
            return knjigaDao.delete(knjiga);
        }
    }
}
