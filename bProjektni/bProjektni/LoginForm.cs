﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bProjektni.view_login;
using bProjektni.data.dto;

namespace bProjektni
{
    public partial class LoginForm : Form
    {
        public AdminDTO user;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login l = new Login();
            user = l.bpLogin(txtUsername.Text, txtPassword.Text);
            if (user == null)
            {
                lblMessage.Text = ("Neuspješna prijava na sistem. Pokušajte ponovo.");
                txtPassword.Text = "";
                lblMessage.Visible = true;
            }
            else
            {
                this.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lblMessage_Click(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
            }
        }
    }
}
