-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2017 at 05:13 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projektni1`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `admin`
-- (See below for the actual view)
--
CREATE TABLE `admin` (
`KorisnickoIme` varchar(255)
,`ImeIPrezime` varchar(255)
,`LozinkaZaPristupSistemu` varchar(255)
,`IDOsobe` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`KorisnickoIme`, `IDOsobe`) VALUES
('mbojic', 18);

-- --------------------------------------------------------

--
-- Stand-in structure for view `administratorview`
-- (See below for the actual view)
--
CREATE TABLE `administratorview` (
`IDOsobe` int(11)
,`KorisnickoIme` varchar(255)
,`ImeIPrezime` varchar(255)
,`Email` varchar(255)
,`DatumRegistracije` timestamp
,`LozinkaZaPristupSistemu` varchar(255)
,`Aktivan` tinyint(1)
,`Adresa` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `aktivni_korisnici`
-- (See below for the actual view)
--
CREATE TABLE `aktivni_korisnici` (
`KorisnickoIme` varchar(255)
,`ImeIPrezime` varchar(255)
,`Email` varchar(255)
,`LozinkaZaPristupSistemu` varchar(255)
,`DatumRegistracije` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `izdavac`
--

CREATE TABLE `izdavac` (
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Adresa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `izdavac`
--

INSERT INTO `izdavac` (`Naziv`, `Adresa`) VALUES
('Glas Srpske', 'Zivoina Misica 11'),
('MBookcase', 'MDujica 22'),
('Prosveta', 'Petra Kocica 12');

-- --------------------------------------------------------

--
-- Table structure for table `izdavac_ima_kontaktosobu`
--

CREATE TABLE `izdavac_ima_kontaktosobu` (
  `UlogaUOrganizaciji` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Napomena` text COLLATE utf8_unicode_ci,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `izvjestaj`
--

CREATE TABLE `izvjestaj` (
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `IDZadatka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jezik`
--

CREATE TABLE `jezik` (
  `Jezik` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jezik`
--

INSERT INTO `jezik` (`Jezik`) VALUES
('Arapski'),
('Engleski'),
('Flamanski'),
('Italijanski'),
('Korejski'),
('Neki novi jezik'),
('Srpski');

-- --------------------------------------------------------

--
-- Table structure for table `jezik_osoba`
--

CREATE TABLE `jezik_osoba` (
  `Jezik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `je_zaduzen_za`
--

CREATE TABLE `je_zaduzen_za` (
  `TipZaduzenja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `IDZadatka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategorija`
--

CREATE TABLE `kategorija` (
  `Naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Opis` text COLLATE utf8_unicode_ci,
  `IDKategorije` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `knjiga`
--

CREATE TABLE `knjiga` (
  `BrojPosjeta` int(11) NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FK_Jezik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DatumIzdavanja` date NOT NULL,
  `DatumKreiranja` date NOT NULL,
  `DatumObjave` date DEFAULT NULL,
  `IDZadatka` int(11) DEFAULT NULL,
  `FK_Izdavac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FK_Komplet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nadzor_KorisnickoIme(FK)` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Nadzor_IDOsobe` int(11) NOT NULL,
  `OdgovornaO_KorisnickoIme(FK)` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OdgovornaO_IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `knjiga`
--

INSERT INTO `knjiga` (`BrojPosjeta`, `IDKnjige`, `Naziv`, `FK_Jezik`, `DatumIzdavanja`, `DatumKreiranja`, `DatumObjave`, `IDZadatka`, `FK_Izdavac`, `FK_Komplet`, `Nadzor_KorisnickoIme(FK)`, `Nadzor_IDOsobe`, `OdgovornaO_KorisnickoIme(FK)`, `OdgovornaO_IDOsobe`) VALUES
(0, 20, 'Kako efikasnije pretrazivati internet', 'Engleski', '2012-04-20', '2012-04-20', NULL, NULL, 'MBookcase', NULL, 'mbojic', 18, 'mbojic', 18),
(0, 21, 'Simpleks tabela', 'Srpski', '2012-06-13', '2012-06-13', NULL, NULL, 'Prosveta', NULL, 'mbojic', 18, 'mbojic', 18),
(0, 22, 'Simplex table', 'Engleski', '2010-02-20', '2010-02-20', NULL, NULL, 'MBookcase', NULL, 'mbojic', 18, 'mbojic', 18);

-- --------------------------------------------------------

--
-- Table structure for table `knjiga_kategorija`
--

CREATE TABLE `knjiga_kategorija` (
  `IDKnjige` int(11) NOT NULL,
  `IDKategorije` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `komentar_ki`
--

CREATE TABLE `komentar_ki` (
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `komplet`
--

CREATE TABLE `komplet` (
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kontakt_osoba`
--

CREATE TABLE `kontakt_osoba` (
  `Adresa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kontakt_osoba`
--

INSERT INTO `kontakt_osoba` (`Adresa`, `IDOsobe`) VALUES
('Milosa Dujica 11', 18),
('postanska', 26);

-- --------------------------------------------------------

--
-- Table structure for table `korisnicka_instanca`
--

CREATE TABLE `korisnicka_instanca` (
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DatumRegistracije` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LozinkaZaPristupSistemu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDOsobe` int(11) NOT NULL,
  `Aktivan` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`KorisnickoIme`, `DatumRegistracije`, `LozinkaZaPristupSistemu`, `IDOsobe`, `Aktivan`) VALUES
('mbojic', '2017-04-20 01:56:54', 'nesto', 18, 1),
('student', '2017-04-20 02:13:01', 'student', 21, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `messages`
-- (See below for the actual view)
--
CREATE TABLE `messages` (
`KorisnickoIme` varchar(255)
,`ImeIPrezime` varchar(255)
,`Vrijeme` timestamp
,`Knjiga` varchar(255)
,`Sadrzaj` text
,`Email` varchar(255)
,`IDOsobe` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `napomena`
--

CREATE TABLE `napomena` (
  `IDRijeci` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDKnjige` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ocjena`
--

CREATE TABLE `ocjena` (
  `Vrijeme` time NOT NULL,
  `Vrijednost` int(11) NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `osoba`
--

CREATE TABLE `osoba` (
  `IDOsobe` int(11) NOT NULL,
  `ImeIPrezime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `osoba`
--

INSERT INTO `osoba` (`IDOsobe`, `ImeIPrezime`, `Email`) VALUES
(18, 'Milan B', 'm@m.m'),
(21, 'Student', 'Student'),
(26, 'Korisnik sa Telefonom', 't@t.t');

-- --------------------------------------------------------

--
-- Table structure for table `podjeljena_sa`
--

CREATE TABLE `podjeljena_sa` (
  `Link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDKnjige` int(11) NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `Izmjene` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `poruka`
--

CREATE TABLE `poruka` (
  `Sadrzaj` text COLLATE utf8_unicode_ci NOT NULL,
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `IDKnjige` int(11) DEFAULT NULL,
  `Odgovorena` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `poruka`
--

INSERT INTO `poruka` (`Sadrzaj`, `Vrijeme`, `KorisnickoIme`, `IDOsobe`, `IDKnjige`, `Odgovorena`) VALUES
('Druga poruka! Odgovorite!', '2017-04-03 22:00:00', 'mbojic', 18, NULL, NULL),
('Prva poruka. Odgovorite!', '2017-04-04 22:00:00', 'mbojic', 18, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posjetio`
--

CREATE TABLE `posjetio` (
  `Vrijeme` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Trajanje` time NOT NULL,
  `KorisnickoIme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IDOsobe` int(11) NOT NULL,
  `IDKnjige` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rijec`
--

CREATE TABLE `rijec` (
  `IDRijeci` int(11) NOT NULL,
  `Rijec` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `Pocetak` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RedniBroj` int(11) NOT NULL,
  `Spektogram` blob,
  `IDKnjige` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telefon_osobe`
--

CREATE TABLE `telefon_osobe` (
  `BrojTelefona` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `TipTelefona` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DatumAzuriranja` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IDOsobe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `telefon_osobe`
--

INSERT INTO `telefon_osobe` (`BrojTelefona`, `TipTelefona`, `DatumAzuriranja`, `IDOsobe`) VALUES
('065319315', 'tip tel', '2017-04-20 02:33:31', 18);

-- --------------------------------------------------------

--
-- Table structure for table `zadatak`
--

CREATE TABLE `zadatak` (
  `IDZadatka` int(11) NOT NULL,
  `Opis` text COLLATE utf8_unicode_ci NOT NULL,
  `DatumPocetka` date NOT NULL,
  `Rok` date NOT NULL,
  `Zavrsen` tinyint(1) NOT NULL,
  `Blokiran` tinyint(1) NOT NULL,
  `BrojPredvidjenihSati` int(11) NOT NULL,
  `BrojUtrosenihSati` int(11) NOT NULL,
  `JosPotrebnihSati` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zadatak`
--

INSERT INTO `zadatak` (`IDZadatka`, `Opis`, `DatumPocetka`, `Rok`, `Zavrsen`, `Blokiran`, `BrojPredvidjenihSati`, `BrojUtrosenihSati`, `JosPotrebnihSati`) VALUES
(1, 'Npraviti novu knjigu - naziv \"Kako efinaksnije pretrazivati internet\"', '2017-04-16', '2017-04-19', 0, 0, 10, 0, 10);

-- --------------------------------------------------------

--
-- Structure for view `admin`
--
DROP TABLE IF EXISTS `admin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `admin`  AS  select `korisnik`.`KorisnickoIme` AS `KorisnickoIme`,`osoba`.`ImeIPrezime` AS `ImeIPrezime`,`korisnik`.`LozinkaZaPristupSistemu` AS `LozinkaZaPristupSistemu`,`osoba`.`IDOsobe` AS `IDOsobe` from ((`osoba` join `korisnik` on((`osoba`.`IDOsobe` = `korisnik`.`IDOsobe`))) join `administrator` on(((`osoba`.`IDOsobe` = `administrator`.`IDOsobe`) and (`korisnik`.`KorisnickoIme` = `administrator`.`KorisnickoIme`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `administratorview`
--
DROP TABLE IF EXISTS `administratorview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `administratorview`  AS  select `osoba`.`IDOsobe` AS `IDOsobe`,`korisnik`.`KorisnickoIme` AS `KorisnickoIme`,`osoba`.`ImeIPrezime` AS `ImeIPrezime`,`osoba`.`Email` AS `Email`,`korisnik`.`DatumRegistracije` AS `DatumRegistracije`,`korisnik`.`LozinkaZaPristupSistemu` AS `LozinkaZaPristupSistemu`,`korisnik`.`Aktivan` AS `Aktivan`,`kontakt_osoba`.`Adresa` AS `Adresa` from (((`osoba` join `korisnik` on((`osoba`.`IDOsobe` = `korisnik`.`IDOsobe`))) join `kontakt_osoba` on((`osoba`.`IDOsobe` = `kontakt_osoba`.`IDOsobe`))) join `administrator` on(((`osoba`.`IDOsobe` = `administrator`.`IDOsobe`) and (`korisnik`.`KorisnickoIme` = `administrator`.`KorisnickoIme`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `aktivni_korisnici`
--
DROP TABLE IF EXISTS `aktivni_korisnici`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aktivni_korisnici`  AS  select `korisnik`.`KorisnickoIme` AS `KorisnickoIme`,`osoba`.`ImeIPrezime` AS `ImeIPrezime`,`osoba`.`Email` AS `Email`,`korisnik`.`LozinkaZaPristupSistemu` AS `LozinkaZaPristupSistemu`,`korisnik`.`DatumRegistracije` AS `DatumRegistracije` from (`osoba` join `korisnik` on((`osoba`.`IDOsobe` = `korisnik`.`IDOsobe`))) where (`korisnik`.`Aktivan` = 1) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `messages`
--
DROP TABLE IF EXISTS `messages`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `messages`  AS  select `p`.`KorisnickoIme` AS `KorisnickoIme`,`osoba`.`ImeIPrezime` AS `ImeIPrezime`,`p`.`Vrijeme` AS `Vrijeme`,`k`.`Naziv` AS `Knjiga`,`p`.`Sadrzaj` AS `Sadrzaj`,`osoba`.`Email` AS `Email`,`p`.`IDOsobe` AS `IDOsobe` from (((`poruka` `p` join `korisnik` on(((`p`.`KorisnickoIme` = `korisnik`.`KorisnickoIme`) and (`p`.`IDOsobe` = `korisnik`.`IDOsobe`)))) join `osoba` on((`p`.`IDOsobe` = `osoba`.`IDOsobe`))) left join `knjiga` `k` on((`p`.`IDKnjige` = `k`.`IDKnjige`))) where ((`p`.`Odgovorena` = 0) or isnull(`p`.`Odgovorena`)) order by `p`.`Vrijeme` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`KorisnickoIme`,`IDOsobe`),
  ADD KEY `administrator_ibfk_2` (`IDOsobe`);

--
-- Indexes for table `izdavac`
--
ALTER TABLE `izdavac`
  ADD PRIMARY KEY (`Naziv`);

--
-- Indexes for table `izdavac_ima_kontaktosobu`
--
ALTER TABLE `izdavac_ima_kontaktosobu`
  ADD PRIMARY KEY (`Naziv`,`IDOsobe`),
  ADD KEY `R_18` (`IDOsobe`);

--
-- Indexes for table `izvjestaj`
--
ALTER TABLE `izvjestaj`
  ADD PRIMARY KEY (`Vrijeme`,`KorisnickoIme`,`IDOsobe`,`IDZadatka`),
  ADD KEY `R_12` (`KorisnickoIme`,`IDOsobe`,`IDZadatka`);

--
-- Indexes for table `jezik`
--
ALTER TABLE `jezik`
  ADD PRIMARY KEY (`Jezik`);

--
-- Indexes for table `jezik_osoba`
--
ALTER TABLE `jezik_osoba`
  ADD PRIMARY KEY (`Jezik`,`IDOsobe`),
  ADD KEY `R_48` (`IDOsobe`);

--
-- Indexes for table `je_zaduzen_za`
--
ALTER TABLE `je_zaduzen_za`
  ADD PRIMARY KEY (`KorisnickoIme`,`IDOsobe`,`IDZadatka`),
  ADD KEY `R_11` (`IDZadatka`);

--
-- Indexes for table `kategorija`
--
ALTER TABLE `kategorija`
  ADD PRIMARY KEY (`IDKategorije`);

--
-- Indexes for table `knjiga`
--
ALTER TABLE `knjiga`
  ADD PRIMARY KEY (`IDKnjige`),
  ADD KEY `R_25` (`IDZadatka`),
  ADD KEY `R_56` (`FK_Izdavac`),
  ADD KEY `R_57` (`FK_Komplet`),
  ADD KEY `R_59` (`FK_Jezik`),
  ADD KEY `Nadzor_KorisnickoIme(FK)` (`Nadzor_KorisnickoIme(FK)`),
  ADD KEY `Nadzor_IDOsobe` (`Nadzor_IDOsobe`),
  ADD KEY `OdgovornaO_KorisnickoIme(FK)` (`OdgovornaO_KorisnickoIme(FK)`),
  ADD KEY `OdgovornaO_IDOsobe` (`OdgovornaO_IDOsobe`);

--
-- Indexes for table `knjiga_kategorija`
--
ALTER TABLE `knjiga_kategorija`
  ADD PRIMARY KEY (`IDKnjige`,`IDKategorije`),
  ADD KEY `R_45` (`IDKategorije`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`Vrijeme`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_53` (`IDKnjige`),
  ADD KEY `R_54` (`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `komentar_ki`
--
ALTER TABLE `komentar_ki`
  ADD PRIMARY KEY (`Vrijeme`,`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_51` (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `komplet`
--
ALTER TABLE `komplet`
  ADD PRIMARY KEY (`Naziv`);

--
-- Indexes for table `kontakt_osoba`
--
ALTER TABLE `kontakt_osoba`
  ADD PRIMARY KEY (`IDOsobe`);

--
-- Indexes for table `korisnicka_instanca`
--
ALTER TABLE `korisnicka_instanca`
  ADD PRIMARY KEY (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_19` (`IDKnjige`),
  ADD KEY `R_20` (`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`KorisnickoIme`,`IDOsobe`),
  ADD KEY `IDOsobe` (`IDOsobe`);

--
-- Indexes for table `napomena`
--
ALTER TABLE `napomena`
  ADD PRIMARY KEY (`IDRijeci`,`KorisnickoIme`,`IDOsobe`,`Vrijeme`,`Naziv`,`IDKnjige`),
  ADD KEY `R_31` (`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_37` (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `ocjena`
--
ALTER TABLE `ocjena`
  ADD PRIMARY KEY (`Vrijeme`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_38` (`IDKnjige`),
  ADD KEY `R_39` (`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `osoba`
--
ALTER TABLE `osoba`
  ADD PRIMARY KEY (`IDOsobe`);

--
-- Indexes for table `podjeljena_sa`
--
ALTER TABLE `podjeljena_sa`
  ADD PRIMARY KEY (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_36` (`KorisnickoIme`,`IDOsobe`);

--
-- Indexes for table `poruka`
--
ALTER TABLE `poruka`
  ADD PRIMARY KEY (`Vrijeme`,`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_40` (`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_41` (`IDKnjige`);

--
-- Indexes for table `posjetio`
--
ALTER TABLE `posjetio`
  ADD PRIMARY KEY (`Vrijeme`,`KorisnickoIme`,`IDOsobe`,`IDKnjige`),
  ADD KEY `R_23` (`KorisnickoIme`,`IDOsobe`),
  ADD KEY `R_24` (`IDKnjige`);

--
-- Indexes for table `rijec`
--
ALTER TABLE `rijec`
  ADD PRIMARY KEY (`IDRijeci`),
  ADD KEY `R_42` (`IDKnjige`);

--
-- Indexes for table `telefon_osobe`
--
ALTER TABLE `telefon_osobe`
  ADD PRIMARY KEY (`BrojTelefona`,`IDOsobe`),
  ADD KEY `R_2` (`IDOsobe`);

--
-- Indexes for table `zadatak`
--
ALTER TABLE `zadatak`
  ADD PRIMARY KEY (`IDZadatka`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategorija`
--
ALTER TABLE `kategorija`
  MODIFY `IDKategorije` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `knjiga`
--
ALTER TABLE `knjiga`
  MODIFY `IDKnjige` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `osoba`
--
ALTER TABLE `osoba`
  MODIFY `IDOsobe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `rijec`
--
ALTER TABLE `rijec`
  MODIFY `IDRijeci` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zadatak`
--
ALTER TABLE `zadatak`
  MODIFY `IDZadatka` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `administrator_ibfk_1` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `administrator_ibfk_2` FOREIGN KEY (`IDOsobe`) REFERENCES `kontakt_osoba` (`IDOsobe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `izdavac_ima_kontaktosobu`
--
ALTER TABLE `izdavac_ima_kontaktosobu`
  ADD CONSTRAINT `R_17` FOREIGN KEY (`Naziv`) REFERENCES `izdavac` (`Naziv`),
  ADD CONSTRAINT `R_18` FOREIGN KEY (`IDOsobe`) REFERENCES `kontakt_osoba` (`IDOsobe`);

--
-- Constraints for table `izvjestaj`
--
ALTER TABLE `izvjestaj`
  ADD CONSTRAINT `R_12` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`,`IDZadatka`) REFERENCES `je_zaduzen_za` (`KorisnickoIme`, `IDOsobe`, `IDZadatka`);

--
-- Constraints for table `jezik_osoba`
--
ALTER TABLE `jezik_osoba`
  ADD CONSTRAINT `R_46` FOREIGN KEY (`Jezik`) REFERENCES `jezik` (`Jezik`),
  ADD CONSTRAINT `R_48` FOREIGN KEY (`IDOsobe`) REFERENCES `osoba` (`IDOsobe`);

--
-- Constraints for table `je_zaduzen_za`
--
ALTER TABLE `je_zaduzen_za`
  ADD CONSTRAINT `R_10` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `administrator` (`KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_11` FOREIGN KEY (`IDZadatka`) REFERENCES `zadatak` (`IDZadatka`);

--
-- Constraints for table `knjiga`
--
ALTER TABLE `knjiga`
  ADD CONSTRAINT `R_25` FOREIGN KEY (`IDZadatka`) REFERENCES `zadatak` (`IDZadatka`),
  ADD CONSTRAINT `R_56` FOREIGN KEY (`FK_Izdavac`) REFERENCES `izdavac` (`Naziv`),
  ADD CONSTRAINT `R_57` FOREIGN KEY (`FK_Komplet`) REFERENCES `komplet` (`Naziv`),
  ADD CONSTRAINT `R_59` FOREIGN KEY (`FK_Jezik`) REFERENCES `jezik` (`Jezik`),
  ADD CONSTRAINT `knjiga_ibfk_1` FOREIGN KEY (`Nadzor_KorisnickoIme(FK)`) REFERENCES `administrator` (`KorisnickoIme`),
  ADD CONSTRAINT `knjiga_ibfk_2` FOREIGN KEY (`Nadzor_IDOsobe`) REFERENCES `administrator` (`IDOsobe`),
  ADD CONSTRAINT `knjiga_ibfk_3` FOREIGN KEY (`OdgovornaO_KorisnickoIme(FK)`) REFERENCES `administrator` (`KorisnickoIme`),
  ADD CONSTRAINT `knjiga_ibfk_4` FOREIGN KEY (`OdgovornaO_IDOsobe`) REFERENCES `administrator` (`IDOsobe`);

--
-- Constraints for table `knjiga_kategorija`
--
ALTER TABLE `knjiga_kategorija`
  ADD CONSTRAINT `R_43` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`),
  ADD CONSTRAINT `R_45` FOREIGN KEY (`IDKategorije`) REFERENCES `kategorija` (`IDKategorije`);

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `R_52` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_53` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`),
  ADD CONSTRAINT `R_54` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `komentar_ki`
--
ALTER TABLE `komentar_ki`
  ADD CONSTRAINT `R_51` FOREIGN KEY (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnicka_instanca` (`Naziv`, `IDKnjige`, `KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `kontakt_osoba`
--
ALTER TABLE `kontakt_osoba`
  ADD CONSTRAINT `kontakt_osoba_ibfk_1` FOREIGN KEY (`IDOsobe`) REFERENCES `osoba` (`IDOsobe`) ON DELETE CASCADE;

--
-- Constraints for table `korisnicka_instanca`
--
ALTER TABLE `korisnicka_instanca`
  ADD CONSTRAINT `R_19` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`),
  ADD CONSTRAINT `R_20` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD CONSTRAINT `korisnik_ibfk_1` FOREIGN KEY (`IDOsobe`) REFERENCES `osoba` (`IDOsobe`) ON DELETE CASCADE;

--
-- Constraints for table `napomena`
--
ALTER TABLE `napomena`
  ADD CONSTRAINT `R_30` FOREIGN KEY (`IDRijeci`) REFERENCES `rijec` (`IDRijeci`),
  ADD CONSTRAINT `R_31` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_37` FOREIGN KEY (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnicka_instanca` (`Naziv`, `IDKnjige`, `KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `ocjena`
--
ALTER TABLE `ocjena`
  ADD CONSTRAINT `R_38` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`),
  ADD CONSTRAINT `R_39` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `podjeljena_sa`
--
ALTER TABLE `podjeljena_sa`
  ADD CONSTRAINT `R_21` FOREIGN KEY (`Naziv`,`IDKnjige`,`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnicka_instanca` (`Naziv`, `IDKnjige`, `KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_36` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`);

--
-- Constraints for table `poruka`
--
ALTER TABLE `poruka`
  ADD CONSTRAINT `R_40` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_41` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`);

--
-- Constraints for table `posjetio`
--
ALTER TABLE `posjetio`
  ADD CONSTRAINT `R_23` FOREIGN KEY (`KorisnickoIme`,`IDOsobe`) REFERENCES `korisnik` (`KorisnickoIme`, `IDOsobe`),
  ADD CONSTRAINT `R_24` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`);

--
-- Constraints for table `rijec`
--
ALTER TABLE `rijec`
  ADD CONSTRAINT `R_42` FOREIGN KEY (`IDKnjige`) REFERENCES `knjiga` (`IDKnjige`);

--
-- Constraints for table `telefon_osobe`
--
ALTER TABLE `telefon_osobe`
  ADD CONSTRAINT `R_2` FOREIGN KEY (`IDOsobe`) REFERENCES `kontakt_osoba` (`IDOsobe`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
